//
//  WebServices.swift
//  Earth_Movement_app
//
//  Created by 3rd Digital-002 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

import UIKit
import Alamofire

let webAPI : WebAPI = WebAPI()

class WebAPI: NSObject {
    /*
    open var shouldShowProgressHUD = true
    
    // MARK: - COUNTRY
    public func GET(shouldShowProgressHUD: Bool = true, param: Dictionary<String,String>, completionBlock:@escaping (([String: Any]?, Error?) -> Void)) {
        
        if shouldShowProgressHUD {
            SVProgressHUD.show()
        }
        
        let endpoint = APIEndPoint.getAllCountries
        
        Alamofire.request(endpoint, method: .get, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            SVProgressHUD.dismiss()
            switch response.result {
            case .success(let value):
                print(value)
                completionBlock(value as? [String: AnyObject], nil)
                break
            case .failure(let error):
                print(error)
                
                completionBlock(nil, error)
                EMUtil.showCardAlertMessage(withMessage: error.localizedDescription, andDuration: 3.0)
                break
            }
        }
    }
    
    // MARK: - REGISTER
    public func UPLOAD(shouldShowProgressHUD: Bool = true, param: Dictionary<String, AnyObject>, completionBlock:@escaping (([String: Any]?, Error?) -> Void)) {
        
        if shouldShowProgressHUD {
            SVProgressHUD.show()
        }
        
        let endpoint = APIEndPoint.register
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            SVProgressHUD.dismiss()
            for (key, value) in param {
                if value is UIImage {
                    let image = value as! UIImage
                    
                    if let data = image.pngRepresentationData {
                        multipartFormData.append(data, withName: key, fileName: "file.png", mimeType: "image/png")
                    } else if let data = image.jpegRepresentationData {
                        multipartFormData.append(data, withName: key, fileName: "file.jpeg", mimeType: "image/jpeg")
                    }
                } else {
                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, to:endpoint)
        { (result) in
            SVProgressHUD.dismiss()
            
            switch result {
                
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                
                upload.responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        print(value)
                        completionBlock(value as? [String: AnyObject], nil)
                        break
                    case .failure(let error):
                        print(error)
                        completionBlock(nil, error)
                        EMUtil.showCardAlertMessage(withMessage: error.localizedDescription, andDuration: 3.0)
                        break
                    }
                }
                
            case .failure(let encodingError):
                //print encodingError.description
                print(encodingError)
                completionBlock(nil, encodingError)
                EMUtil.showCardAlertMessage(withMessage: encodingError.localizedDescription, andDuration: 3.0)
                break
            }
        }
    }
*/
}
