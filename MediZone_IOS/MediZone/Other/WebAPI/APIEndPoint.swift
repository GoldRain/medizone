//
//  EMWebServiceAPI.swift
//  Earth_Movement_app
//
//  Created by 3rdDigital-001 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

import UIKit

struct APIEndPoint {
    
    static let isLive : Bool = true
    
    //TODO: BASE URL FOR LIVE
    static let baseURLDEV = "http://dev.3rddigital.com/earthmovement"
    static let baseURLLIVE = "http://earthmovementapp.com"
    
    //////
    static let baseURL = (isLive==true) ? baseURLLIVE : baseURLDEV
    
    static let baseUrlAPI = baseURL + "/api/"
    
    static let imageListingURL = baseURL + "/listing-img/"
    
    static let imageProfileURL = baseURL + "/profile-img/"
    
    //// API
    static let getAllCountries = APIEndPoint.baseUrlAPI + "Get_All_Country"
}
