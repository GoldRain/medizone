//
//  AppMessages.swift
//  Earth_Movement_app
//
//  Created by 3rd Digital-002 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

/// LOGIN
let MSG_NO_INTERNET_CONNECTION = "There is no internet connection."
let MSG_NO_EMAIL = "Please enter your email."
let MSG_NO_PASSWORD = "Please enter your password."
let MSG_INVALID_EMAIL = "Please enter valid email."
let MSG_INCORRECT_PASSWORD = "Please enter correct password."
let MSG_INVALID_PASSWORD_CHAR = "Password must be minimum 6 and maximum 20 character long."
let MSG_VERIFY_PASSWORD_INCORRECT = "Password and Verify password do not match."
let MSG_LOGOUT = "Are you sure you want to logout?"


/// REGISTER -> LOGIN
let MSG_NO_NAME = "Please enter your name."
let MSG_NO_STREET_ADDRESS = "Please enter street address."
let MSG_NO_CITY = "Please enter city."
let MSG_NO_COUNTRY = "Please select country."
let MSG_NO_STATE = "Please select state."
let MSG_NO_ZIPCODE = "Please enter zipcode."
let MSG_INVALID_ZIPCODE = "Please enter valid zipcode."
let MSG_NO_EXTENDEX_ACCOUNT_PROFILE = "Please enter extended account/profile."
let MSG_NO_PHONE_NUMBER = "Please enter valid phone number."
let MSG_NO_ABOUT_SUPPLIER = "Please enter about supplier."
let MSG_NO_FACEBOOK_ADDRESS = "Please enter facebook address."
let MSG_NO_TWITTER_ADDRESS = "Please enter twitter name."
let MSG_NO_PROFILE_IMAGE = "Please add profile image."
let MSG_NO_ADDRESS = "Please enter address."

/// DIRT TYPE -> REGISTER
let MSG_FILL_DIRT_TYPE = "Please fill dirt type."
let MSG_SELECT_DATE = "Please select date."
let MSG_ENTER_TITLE = "Please enter title."
let MSG_ENTER_DESCRIPTION = "Please enter description."
let MSG_ENTER_FEES = "Please enter fees."
let MSG_SELECT_DATE_AVAILABLE = "Please select Available date ."
let MSG_SELECT_DATE_AVAILABLE_UNTIL = "Please select until Available date ."
let MSG_IMAGE_CROP = "Image size less then 2 MB."


/// RESET PASSWORD -> LOGIN
let MSG_NO_OLD_PASSWORD = "Please enter your old password."
let MSG_NO_NEW_PASSWORD = "Please enter your new password."
let MSG_NO_RETYPE_PASSWORD = "Please enter retype password."
let MSG_NO_VERIFY_PASSWORD_INCORRECT = "New password and retype password do not match."


//API MESSAGES
let MSG_PROFILE_UPDATE_SUCCESSFULLY = "Profile updated successfully."
let MSG_REG_SUCCESSFULLY = "Register successfully."

let MSG_SESSION_EXPIRED = "Session has been expired."
let MSG_DIRT_ADDED_SUCCESSFULLY = "Dirt Added Successfully."
let MSG_DATE_NOT_GRATER = "Avalable until must greater that avalable date."
let MSG_UPDATE_FOLLOWERS = "Update follow successfully"
let MSG_UPDATE_BOOKMARK = "Bookmark successfully added."
let MSG_UPDATE_BOOKMARK_FOLLOWERS = "Bookmark followers status."
let MSG_DIRT_UPDATE_SUCCESSFULLY = "Dirt update successfully."


//GENERAL

let MSG_GUEST_LOGIN = "Login Required."









