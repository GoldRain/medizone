//
//  EMTheme.swift
//  Earth_Movement_app
//
//  Created by 3rdDigital-001 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

import UIKit

struct AppTheme {
    
    struct Navigation {
        static let backgroundColour = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        static let foregroundColour = UIColor.init(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
}
