//
//  EMUtil.swift
//  Earth_Movement_app
//
//  Created by 3rd Digital-002 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenu

class AppUtil: NSObject {
    
    struct storyBoard {
        
        private init(){}
        static let home = UIStoryboard.init(name: "Home", bundle: nil)
        static let tutorial = UIStoryboard.init(name: "Tutorial", bundle: nil)
    }
    
    class var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    
    class func is_iPad() -> Bool {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            return true
        } else {
            return false
        }
    }
    
    static func getTopViewController() -> UIViewController {
        
        var viewController = UIViewController()
        
        if let vc =  UIApplication.shared.delegate?.window??.rootViewController {
            
            viewController = vc
            var presented = vc
            
            while let top = presented.presentedViewController {
                presented = top
                viewController = top
            }
        }
        
        return viewController
    }
    
    class func localToUTC(date:String, fromFormat: String, toFormat: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.date
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    class func UTCToLocal(date:String, fromFormat: String, toFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = toFormat
        
        return dateFormatter.string(from: dt!)
    }
    
    class func createHomeSideMenu(navigationController: UINavigationController?) {
        // Define the menus
        
        let leftViewController = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: SideMenuVC.identifier) as! SideMenuVC
        
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: leftViewController)
        
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        //SideMenuManager.default.menuAddPanGestureToPresent(toView: navigationController!.navigationBar)
        //SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: navigationController!.view)
        
        SideMenuManager.defaultManager.menuFadeStatusBar = false
        
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width * 0.8
    }
}
