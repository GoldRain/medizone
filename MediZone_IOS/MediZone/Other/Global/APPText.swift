//
//  EMText.swift
//  Earth_Movement_app
//
//  Created by 3rd Digital-002 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

import UIKit

class APPText: NSObject {
    struct Label {
        static let appName     = "Earth Movement"
        static let Ok          = "OK"
        static let Done        = "Done"
        static let Cancel      = "Cancel"
        static let fromCamera  = "Take Photo"
        static let fromLibrary = "From Albums"
    }
}
