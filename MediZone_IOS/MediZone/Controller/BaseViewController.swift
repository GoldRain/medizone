//
//  EMViewController.swift
//  Earth_Movement_app
//
//  Created by 3rd Digital-002 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwifterSwift
import SideMenu

class BaseViewController: UIViewController,UIGestureRecognizerDelegate {
    
    open var shouldHideNavigationBar = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        if shouldHideNavigationBar {
            self.navigationController?.isNavigationBarHidden = true
        }
        
        
        //setNavigationLogo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        if shouldHideNavigationBar {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.endEditing(true)
        if shouldHideNavigationBar {
            self.navigationController?.isNavigationBarHidden = false
        }
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self;
    
    }
    
    open func setNavigationTitle(title: String, backgroundColour: UIColor, titleColour: UIColor) {
        self.navigationController?.navigationBar.backItem?.title = title
        self.navigationController?.navigationBar.tintColor = backgroundColour
        self.navigationController?.navigationBar.barTintColor = backgroundColour
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):titleColour, NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue):UIFont(name:"Comfortaa-Regular", size:20)!]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.title = title
    }
    
    open func setNavigationLogo(name: String) {
        
        let logo = UIImage(named: name)
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - Keyboard Hide
    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.resignFirstResponder()
        return false
    }
    
    func logOutBtnClick() {
        //let currentUser = CurrentUser.getLoginData()
        //CurrentUser.removeLoginData(logindata: currentUser)
        //LoginNavigationViewController.showAsRootView(shouldShowAnimation: true)
        print("logOut button tapped")
    }
}
