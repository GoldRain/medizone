//
//  IAPVC.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu

class IAPVC: BaseViewController {
    
    //MARK: - VAR
    public static let identifier = "IAPVC"
     @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            if #available(iOS 11.0, *) {
                statusBarHeight.constant = UIApplication.shared.keyWindow!.safeAreaInsets.top
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //setNavigationLogo(name: "navigation-header")
        
        SideMenuManager.defaultManager.menuEnableSwipeGestures = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController({
            
        })
    }
    
    //MARK:- Helper Method
    class func create() -> IAPVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: IAPVC.identifier) as! IAPVC
        
        return vc
    }
}
