//
//  PlayerVC.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit
import Presentr
import SideMenu

class PlayerVC: BaseViewController {
    
    //MARK: - VAR
    public static let identifier = "PlayerVC"
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    let presenter: Presentr = {
        let width = ModalSize.full
        let height = ModalSize.full
        let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: 0))
        let customType = PresentationType.custom(width: width, height: height, center: center)
        
        let customPresenter = Presentr(presentationType: customType)
        customPresenter.transitionType = .coverVerticalFromTop
        customPresenter.dismissTransitionType = .crossDissolve
        customPresenter.roundCorners = false
        customPresenter.backgroundColor = .black
        customPresenter.backgroundOpacity = 0.8
        customPresenter.dismissOnSwipe = true
        customPresenter.dismissOnSwipeDirection = .top
        return customPresenter
    }()
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            if #available(iOS 11.0, *) {
                statusBarHeight.constant = UIApplication.shared.keyWindow!.safeAreaInsets.top
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //setNavigationLogo(name: "navigation-header")
        SideMenuManager.defaultManager.menuEnableSwipeGestures = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController({
            
        })
    }
    
    @IBAction func timerTapped(_ sender: UIButton) {
        navigationController?.pushViewController(IAPVC.create())
    }
    
    @IBAction func questionMarkTapped(_ sender: UIButton) {
        
        let vc = WhatIsAutoPlayVC.create()
        
        self.customPresentViewController(self.presenter, viewController: vc, animated: true, completion: nil)
    }
    
    @IBAction func previousTapped(_ sender: UIButton) {
                
        let vc = GotFreeMinutesVC.create()
        
        self.customPresentViewController(self.presenter, viewController: vc, animated: true, completion: nil)
    }
    
    @IBAction func nextTapped(_ sender: UIButton) {
        
        let vc = OutofTimeVC.create()
        
        self.customPresentViewController(self.presenter, viewController: vc, animated: true, completion: nil)
    }
    
    @IBAction func playTapped(_ sender: UIButton) {
        
        self.navigationController?.pushViewController(IAPVC.create())
    }
    
    //MARK:- Helper Method
    class func create() -> PlayerVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: PlayerVC.identifier) as! PlayerVC
        
        return vc
    }
}
