//
//  HomeNaVC.swift
//  MediZone
//
//  Created by 001 on 09/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit

class HomeNaVC: UINavigationController {
    
    //MARK: - VAR
    public static let identifier = "HomeNaVC"
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    
    
    //MARK:- Helper Method
    class func create() -> HomeNaVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: HomeNaVC.identifier) as! HomeNaVC
        
        return vc
    }
    
    class func showAsRootView(shouldShowAnimation: Bool = false) -> HomeNaVC? {
        
        guard let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window else {
            return nil
        }
        
        let vc = HomeNaVC.create()
        
        window.rootViewController = vc
        
        if  shouldShowAnimation {
            let snapshot:UIView = (window.snapshotView(afterScreenUpdates: true))!
            vc.view.addSubview(snapshot)
            
            UIView.animate(withDuration: 0.3, animations: {() in
                snapshot.layer.opacity = 0
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
            }, completion: {
                (value: Bool) in
                snapshot.removeFromSuperview()
            })
        }
        
        return vc
    }
}
