//
//  HomeVC.swift
//  MediZone
//
//  Created by 001 on 09/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu

class HomeVC: BaseViewController {
    
    //MARK: - VAR
    public static let identifier = "HomeVC"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let categories = [["title":"Top\nMeditations","image":"pray"],
                      ["title":"New\nMeditations","image":"pray"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"]]
    
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            if #available(iOS 11.0, *) {
                statusBarHeight.constant = UIApplication.shared.keyWindow!.safeAreaInsets.top
            } else {
                // Fallback on earlier versions
            }
        }
        
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    @objc func handleButton( sender : Any ) {
        // It would be nice is isEnabled worked...
        self.navigationController?.pushViewController(IAPVC.create())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.view)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func timerTapped(_ sender: UIButton) {
        navigationController?.pushViewController(IAPVC.create())
    }
    
    @IBAction func settingTapped(_ sender: UIButton) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    //MARK:- Helper Method
    class func create() -> HomeVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: HomeVC.identifier) as! HomeVC
        
        return vc
    }
}

extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategaryCVCell", for: indexPath) as! CategaryCVCell
        
        let category = categories[indexPath.row]
        
        if indexPath.row > 1 {
            cell.topViewCellContainer.isHidden = true
            cell.dynamicCellContainerView.isHidden = false
            
            cell.dynamicCellTitle.text = category["title"]
            cell.imgDynamicCellImage.image = UIImage.init(named: category["image"]!)
            
        } else {
            cell.topViewCellContainer.isHidden = false
            cell.dynamicCellContainerView.isHidden = true
            
            cell.lblTopviewCellTitle.text = category["title"]
            cell.imgTopViewCellImage.image = UIImage.init(named: category["image"]!)
        }
        
        //cell.circleView.clipsToBounds = true
        //cell.circleView.cornerRadius = cell.circleView.size.width/2
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.collectionView.frame.width / 2 - 10, height: self.collectionView.frame.width / 3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if  indexPath.row < 2 {
            let vc = MeditationsListVC.create()
            self.navigationController?.pushViewController(vc)
        } else {
            let vc = SubCategoryVC.create()
            self.navigationController?.pushViewController(vc)
        }
    }
}

