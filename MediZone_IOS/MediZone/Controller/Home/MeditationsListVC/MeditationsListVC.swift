//
//  MeditationsListVC.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu

class MeditationsListVC: BaseViewController {
    
    //MARK: - VAR
    public static let identifier = "MeditationsListVC"
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            if #available(iOS 11.0, *) {
                statusBarHeight.constant = UIApplication.shared.keyWindow!.safeAreaInsets.top
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //setNavigationLogo(name: "navigation-header")
        SideMenuManager.defaultManager.menuEnableSwipeGestures = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController({
            
        })
    }
    
    @IBAction func timerTapped(_ sender: UIButton) {
        navigationController?.pushViewController(IAPVC.create())
    }
    
    //MARK:- Helper Method
    class func create() -> MeditationsListVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: MeditationsListVC.identifier) as! MeditationsListVC
        
        return vc
    }
}

extension MeditationsListVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeditationsListTVC") as! MeditationsListTVC
        
        cell.lblNo.text = (indexPath.row + 1).string
        cell.lblNoSmall.text = (indexPath.row + 1).string
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = PlayerVC.create()
        self.navigationController?.pushViewController(vc)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
