//
//  OutofTimeVC.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu

class OutofTimeVC: UIViewController {
    
    //MARK: - VAR
    public static let identifier = "OutofTimeVC"
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func closeTapped(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
        }
    }
    
    
    //MARK:- Helper Method
    class func create() -> OutofTimeVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: OutofTimeVC.identifier) as! OutofTimeVC
        
        return vc
    }
}
