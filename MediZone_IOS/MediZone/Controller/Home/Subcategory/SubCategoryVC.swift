//
//  SubCategoryVC.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu

class SubCategoryVC: BaseViewController {
    
    //MARK: - VAR
    public static let identifier = "SubCategoryVC"
    
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    let categories = [["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"],
                      ["title":"Demo","image":"p6"]]
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationLogo(name: "navigation-header")
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            if #available(iOS 11.0, *) {
                statusBarHeight.constant = UIApplication.shared.keyWindow!.safeAreaInsets.top
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        SideMenuManager.defaultManager.menuEnableSwipeGestures = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func backButtonTapped(_ sender: UIButton) {
        navigationController?.popViewController({
            
        })
    }
    
    @IBAction func timerTapped(_ sender: UIButton) {
        navigationController?.pushViewController(IAPVC.create())
    }
    
    
    //MARK:- Helper Method
    class func create() -> SubCategoryVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: SubCategoryVC.identifier) as! SubCategoryVC
        
        return vc
    }
}

extension SubCategoryVC: UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategaryCVCell", for: indexPath) as! CategaryCVCell
        
        let category = categories[indexPath.row]
        
        cell.dynamicCellTitle.text = category["title"]
        cell.imgDynamicCellImage.image = UIImage.init(named: category["image"]!)
        
        //cell.circleView.clipsToBounds = true
        //cell.circleView.cornerRadius = cell.circleView.size.width/2
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: self.collectionView.frame.width / 2 - 10, height: self.collectionView.frame.width / 3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = MeditationsListVC.create()
        
        self.navigationController?.pushViewController(vc)
    }
}
