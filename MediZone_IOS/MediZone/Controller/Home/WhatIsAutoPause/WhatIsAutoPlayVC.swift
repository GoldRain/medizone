//
//  WhatIsAutoPlayVC.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit

class WhatIsAutoPlayVC: UIViewController {
    
    //MARK: - VAR
    public static let identifier = "WhatIsAutoPlayVC"
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func closeIcon(_ sender: UIButton) {
        dismiss(animated: true) {
            
        }
    }
    
    
    //MARK:- Helper Method
    class func create() -> WhatIsAutoPlayVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: WhatIsAutoPlayVC.identifier) as! WhatIsAutoPlayVC
        
        return vc
    }
}
