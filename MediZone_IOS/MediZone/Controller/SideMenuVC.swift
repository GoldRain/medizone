//
//  SideMenuVC.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu
import MessageUI

class SideMenuVC: BaseViewController {
    
    //MARK: - VAR
    public static let identifier = "SideMenuVC"
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            if #available(iOS 11.0, *) {
                statusBarHeight.constant = UIApplication.shared.keyWindow!.safeAreaInsets.top
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    
    @IBAction func closeAction(_ sender: UIButton) {
        dismiss(animated: true) {
            
        }
    }
    
    @IBAction func notificationAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            let alert = UIAlertController(title: "Notifiactions",
                                          message: "Do you want to open notification settings?", preferredStyle: UIAlertControllerStyle.alert)
            AppUtil.getTopViewController().present(alert, animated: true, completion: nil)
            
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                DispatchQueue.main.async {
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                    }
                    
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                print("Settings opened: \(success)") // Prints true
                            })
                        } else {
                            UIApplication.shared.openURL(settingsUrl as URL)
                        }
                    }
                }
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { action in
                
            }))
        }
    }
    
    
    @IBAction func restorePurchaseAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            let alert = UIAlertController(title: "Notifiactions",
                                          message: "Do you want restore your purchase?",
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            AppUtil.getTopViewController().present(alert, animated: true, completion: nil)
            
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                DispatchQueue.main.async {

                }
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { action in
                
            }))
        }
    }
    
    @IBAction func requestAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            let alert = UIAlertController(title: "Request",
                                          message: "Do you want to email request?",
                                          preferredStyle: UIAlertControllerStyle.alert)
            
            AppUtil.getTopViewController().present(alert, animated: true, completion: nil)
            
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                DispatchQueue.main.async {
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self
                        mail.setToRecipients(["request@gmail.com"])
                        mail.setSubject("Request")
                        
                        AppUtil.getTopViewController().present(mail, animated: true)
                    } else {
                        // show failure alert
                        
                    }
                }
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { action in
                
            }))
        }
    }
    
    @IBAction func contactUSAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            let alert = UIAlertController(title: "Contact us",
                                          message: "Do you want to contact us?", preferredStyle: UIAlertControllerStyle.alert)
            AppUtil.getTopViewController().present(alert, animated: true, completion: nil)
            
            alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                DispatchQueue.main.async {
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self
                        mail.setToRecipients(["request@gmail.com"])
                        mail.setSubject("Contact us")
                        AppUtil.getTopViewController().present(mail, animated: true)
                    } else {
                        // show failure alert
                    }
                }
            }))
            
            alert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { action in
                
            }))
        }
    }
    
    @IBAction func moreAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            DispatchQueue.main.async {
                UIApplication.shared.openURL(URL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")!)
            }
        }
    }
    
    //MARK:- Helper Method
    class func create() -> SideMenuVC {
        
        let vc = AppUtil.storyBoard.home.instantiateViewController(withIdentifier: SideMenuVC.identifier) as! SideMenuVC
        
        return vc
    }
}

extension SideMenuVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            
        }
    }
}
