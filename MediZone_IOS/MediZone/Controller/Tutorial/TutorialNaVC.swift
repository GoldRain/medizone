//
//  TutorialNavigationViewController.swift
//  PhotoLead
//
//  Created by 3rdDigital-001 on 24/10/17.
//  Copyright © 2017 3rd Digital. All rights reserved.
//

import UIKit

class TutorialNaVC: UINavigationController {
    
    //MARK: - VAR
    public static let identifier = "TutorialNaVC"
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = AppTheme.Navigation.backgroundColour
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    
    
    //MARK:- Helper Method
    class func create() -> TutorialNaVC {
        
        let tutorialNavigationViewController = AppUtil.storyBoard.tutorial.instantiateViewController(withIdentifier: TutorialNaVC.identifier) as! TutorialNaVC
        
        return tutorialNavigationViewController
    }
    
    class func showAsRootView(shouldShowAnimation: Bool = false) -> TutorialNaVC? {
        
        guard let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window else {
            return nil
        }
        
        let tutorialNavigationViewController = TutorialNaVC.create()
        
        let snapshot:UIView = (window.snapshotView(afterScreenUpdates: true))!
        tutorialNavigationViewController.view.addSubview(snapshot)
        
        window.rootViewController = tutorialNavigationViewController
        
        if  !shouldShowAnimation {
            snapshot.removeFromSuperview()
            return tutorialNavigationViewController
        }
        
        UIView.animate(withDuration: 0.3, animations: {() in
            snapshot.layer.opacity = 0
            snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
        }, completion: {
            (value: Bool) in
            snapshot.removeFromSuperview()
        })
        
        return tutorialNavigationViewController
    }
}
