//
//  TutorialPageViewController.swift
//  PhotoLead
//
//  Created by 3rdDigital-001 on 24/10/17.
//  Copyright © 2017 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu

class TutorialPageVC: UIPageViewController {
    
    var pages = [UIViewController]()
    
    public var pageControllerSwipped:((Int) -> Void)?
    
    func pageControllerSwipped(callback:@escaping (Int) -> Void) {
        pageControllerSwipped = callback
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.dataSource = self
        
        let page1: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "TutorialOneVC")
        let page2: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "TutorialTwoVC")
        let page3: UIViewController! = storyboard?.instantiateViewController(withIdentifier: "TutorialThreeVC")
        
        pages.append(page1)
        pages.append(page3)
        pages.append(page2)
        
        self.title = "MECANICÁ"
        
        setViewControllers([page1], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    
    @IBAction func menuTapped(_ sender: UIBarButtonItem) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
}

extension TutorialPageVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        //For infite
        /*
         let currentIndex = pages.index(of: viewController)!
         let previousIndex = abs((currentIndex - 1) % pages.count)
         return pages[previousIndex]
         */
        
        let currentIndex = pages.index(of: viewController)!
        
        let previousIndex = currentIndex - 1
        
        if previousIndex < 0 {
            return nil
        }
        
        return pages[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        //For infite
        /*
         let currentIndex = pages.index(of: viewController)!
         let nextIndex = abs((currentIndex + 1) % pages.count)
         return pages[nextIndex]
         */
        
        let currentIndex = pages.index(of: viewController)!
        let nextIndex = currentIndex + 1
        
        if nextIndex > pages.count - 1 {
            return nil
        }
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        

        if (!completed) {
            return
        }
        
        pageControllerSwipped!(self.viewControllers!.first!.view.tag)
        
    }
}
