//
//  PageContainerViewController.swift
//  PhotoLead
//
//  Created by 3rdDigital-001 on 27/10/17.
//  Copyright © 2017 3rd Digital. All rights reserved.
//

import UIKit
import SideMenu
import SwifterSwift

class PageContainerVC: UIViewController {
    
    //MARK: - VAR
    public static let identifier = "PageContainerVC"
    weak var tutorialPageViewController:TutorialPageVC?
    var rightBarButtonItem : UIBarButtonItem!
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnNext.isHidden = true
        
        SwifterSwift.delay(milliseconds: 100) {
            self.updatePageControl()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "TutorialPageViewController") {
            tutorialPageViewController = segue.destination as? TutorialPageVC
            
            tutorialPageViewController?.pageControllerSwipped(callback: { (index) in

                self.pageControl.currentPage = index
                self.updatePageControl()
                if index > 1{
                    self.btnNext.isHidden = false
                } else {
                    self.btnNext.isHidden = true
                }
    
            })
        }
    }
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    @IBAction func menuTapped(_ sender: Any) {
        
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func skipTapped(_ sender: UIButton) {
        _ = AppUtil.createHomeSideMenu(navigationController: HomeNaVC.showAsRootView(shouldShowAnimation: false))
    }
    
    @IBAction func nextAction(_ sender: UIButton) {
        _ = AppUtil.createHomeSideMenu(navigationController: HomeNaVC.showAsRootView(shouldShowAnimation: false))
    }
    
    //MARK:- Helper Method
    class func create() -> PageContainerVC {
        
        let pageContainerViewController = AppUtil.storyBoard.tutorial.instantiateViewController(withIdentifier: PageContainerVC.identifier) as! PageContainerVC
        
        return pageContainerViewController
    }
    
    @objc func doneAction() {
        //APPUtil.createSideMenu(navigationController: CreateLeadNavigationViewController.showAsRootView(shouldShowAnimation: false))
    }
    
    func updatePageControl() {
        for (index, dot) in pageControl.subviews.enumerated() {
            if index == pageControl.currentPage {
                dot.backgroundColor = .white
                dot.layer.cornerRadius = dot.frame.size.height / 2;
            } else {
                dot.backgroundColor = UIColor.clear
                dot.layer.cornerRadius = dot.frame.size.height / 2
                dot.layer.borderColor = UIColor.white.cgColor
                dot.layer.borderWidth = 1
            }
        }
    }
}
