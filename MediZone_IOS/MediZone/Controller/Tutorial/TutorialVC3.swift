//
//  TutorialVC3.swift
//  MediZone
//
//  Created by 001 on 09/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit

class TutorialVC3: UIViewController {

    //MARK: - VAR
    public static let identifier = "TutorialVC3"
    
    @IBOutlet weak var topView: UIView!
    
    //MARK: - VIEWCONTROLLER LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //topView.clipsToBounds = true
        //topView.layer.cornerRadius = topView.frame.size.width/2

        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect:self.topView.bounds,
                                    byRoundingCorners:[.bottomRight, .bottomLeft],
                                    cornerRadii: CGSize(width: self.topView.frame.size.width/3, height:  self.topView.frame.size.width/3 ))
            
            let maskLayer = CAShapeLayer()
            
            maskLayer.path = path.cgPath
            self.topView.layer.mask = maskLayer
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("\(type(of: self)) released from memory!")
    }
    
    //MARK: - ACTIONS
    
    
    //MARK:- Helper Method

}
