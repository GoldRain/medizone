//
//  LoginUser.swift
//  Earth_Movement_app
//
//  Created by 3rdDigital-001 on 20/09/17.
//  Copyright © 2017 3rd Digital-002. All rights reserved.
//

import UIKit

class CurrentUser: NSObject,NSCoding {
    
    var id: String?
    var token: String?
    var device_token: String = "0"
    var email: String?
    var profile_picture : String?
    var name : String?
    var isProfileUpdated: Int?
    var street : String?
    var city : String?
    var state: String?
    var country: String?
    var zipcode : String?
    var lattitude: String?
    var longitude: String?
    var phone_no: String?
    var extended_account : String?
    var aboutSupplier : String?
    var twitter_name : String?
    var role_id : Int?
    var is_active : Int?
    var created_at: String?
    var updated_at: String?
    var facebook_address : String?
    var countryID: Int?
    
    override init() {
        self.id = ""
        self.token = ""
        self.email = ""
        self.profile_picture = ""
        self.name = ""
        self.device_token = "0"
        self.street = ""
        self.city = ""
        self.state = ""
        self.country = ""
        self.zipcode = ""
        self.lattitude = ""
        self.longitude = ""
        self.phone_no = ""
        self.extended_account = ""
        self.aboutSupplier = ""
        self.twitter_name = ""
        self.role_id = 0
        self.is_active = 0
        self.created_at = ""
        self.updated_at = ""
        self.facebook_address = ""
        self.countryID = 0
    }
    
    //MARK: NSCoding
    required convenience init(coder aDecoder: NSCoder) {
        
        self.init();
        self.device_token = aDecoder.decodeObject(forKey: "device_token") as! String
        self.name = aDecoder.decodeObject(forKey: "user_name") as? String
        self.id = aDecoder.decodeObject(forKey: "user_id") as? String
        self.token = aDecoder.decodeObject(forKey: "token") as? String
        if self.token == nil {
            self.token = "0"
        }
        self.email = aDecoder.decodeObject(forKey: "user_email") as? String
        self.profile_picture = aDecoder.decodeObject(forKey: "user_profile") as? String
        self.street = aDecoder.decodeObject(forKey: "user_street") as? String
        self.city = aDecoder.decodeObject(forKey: "user_city") as? String
        self.state = aDecoder.decodeObject(forKey: "user_state") as? String
        self.country = aDecoder.decodeObject(forKey: "user_country") as? String
        self.zipcode = aDecoder.decodeObject(forKey: "user_zipcode") as? String
        self.lattitude = aDecoder.decodeObject(forKey: "user_lattitude") as? String
        self.longitude = aDecoder.decodeObject(forKey: "user_longitude") as? String
        self.phone_no = aDecoder.decodeObject(forKey: "user_phone_no") as? String
        self.extended_account = aDecoder.decodeObject(forKey: "user_extended_account") as? String
        self.aboutSupplier = aDecoder.decodeObject(forKey: "user_aboutSupplier") as? String
        self.twitter_name = aDecoder.decodeObject(forKey: "user_twitter_name") as? String
        
        self.role_id = aDecoder.decodeObject(forKey: "user_role_id") as? Int
        self.is_active = aDecoder.decodeObject(forKey: "user_is_active") as? Int
        
        self.created_at = aDecoder.decodeObject(forKey: "user_created_at") as? String
        self.updated_at = aDecoder.decodeObject(forKey: "user_updated_at") as? String
        self.facebook_address = aDecoder.decodeObject(forKey: "user_facebook_address") as? String
        self.countryID = aDecoder.decodeObject(forKey: "user_countryID") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id!, forKey: "user_id")
        aCoder.encode(token, forKey: "token")
        aCoder.encode(email, forKey: "user_email")
        aCoder.encode(profile_picture, forKey: "user_profile")
        aCoder.encode(name, forKey: "user_name")
        aCoder.encode(device_token, forKey: "device_token")
        aCoder.encode(street, forKey: "user_street")
        aCoder.encode(city, forKey: "user_city")
        aCoder.encode(state, forKey: "user_state")
        aCoder.encode(country, forKey: "user_country")
        aCoder.encode(zipcode, forKey: "user_zipcode")
        aCoder.encode(lattitude, forKey: "user_lattitude")
        aCoder.encode(longitude, forKey: "user_longitude")
        aCoder.encode(phone_no, forKey: "user_phone_no")
        aCoder.encode(extended_account, forKey: "user_extended_account")
        aCoder.encode(aboutSupplier, forKey: "user_aboutSupplier")
        aCoder.encode(twitter_name, forKey: "user_twitter_name")
        aCoder.encode(role_id, forKey: "user_role_id")
        aCoder.encode(is_active, forKey: "user_is_active")
        aCoder.encode(created_at, forKey: "user_created_at")
        aCoder.encode(updated_at, forKey: "user_updated_at")
        aCoder.encode(facebook_address, forKey: "user_facebook_address")
        aCoder.encode(countryID, forKey: "user_countryID")
    }
    
    class func getLoginData() -> CurrentUser? {
        let userDefaults = UserDefaults.standard
        if let UDdata = userDefaults.object(forKey: "loginuser") {
            let logindata = NSKeyedUnarchiver.unarchiveObject(with: UDdata as! Data)
            
            return logindata as? CurrentUser
            
        } else {
            
            return CurrentUser()
        }
    }
    
    class func isLoggedIn() -> Bool{
        
        guard let currentUser = getLoginData() else {
            return false
        }
        
        guard let currentUserId = currentUser.id else {
            return false
        }
        
        let count = currentUserId.count
        
        return count > 0
    }
    
    class func saveLoginData(logindata: CurrentUser?) {
        
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: logindata as Any)
        
        let userDefaults: UserDefaults = UserDefaults.standard
        userDefaults.set(encodedData, forKey: "loginuser")
        userDefaults.synchronize()
    }
    
    class func removeLoginData(logindata: CurrentUser?) {
        
        logindata?.id = ""
        logindata?.token = ""
        logindata?.email = ""
        logindata?.profile_picture = ""
        logindata?.name = ""
        logindata?.street = ""
        logindata?.city = ""
        logindata?.state = ""
        logindata?.country = ""
        logindata?.lattitude = ""
        logindata?.longitude = ""
        logindata?.phone_no = ""
        logindata?.extended_account = ""
        logindata?.aboutSupplier = ""
        logindata?.twitter_name = ""
        logindata?.role_id = 0
        logindata?.is_active = 0
        logindata?.created_at = ""
        logindata?.updated_at = ""
        logindata?.facebook_address = ""
        logindata?.countryID = 0
        
        saveLoginData(logindata: logindata)
    }
}
