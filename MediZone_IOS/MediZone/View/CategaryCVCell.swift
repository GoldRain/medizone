//
//  CategaryCollectionViewCell.swift
//  MediZone
//
//  Created by 001 on 09/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit

class CategaryCVCell: UICollectionViewCell {
    
    @IBOutlet weak var topViewCellContainer: UIView!
    @IBOutlet weak var dynamicCellContainerView: UIView!
    
    @IBOutlet weak var lblTopviewCellTitle: UILabel!
    @IBOutlet weak var imgTopViewCellImage: UIImageView!
    
    @IBOutlet weak var imgDynamicCellImage: UIImageView!
    @IBOutlet weak var dynamicCellTitle: UILabel!
}
