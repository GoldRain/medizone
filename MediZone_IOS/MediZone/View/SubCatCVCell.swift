//
//  SubCatCVCell.swift
//  MediZone
//
//  Created by 001 on 10/03/18.
//  Copyright © 2018 3rd Digital. All rights reserved.
//

import UIKit

class SubCatCVCell: UICollectionViewCell {
    @IBOutlet weak var dynamicCellContainerView: UIView!
    
    @IBOutlet weak var imgDynamicCellImage: UIImageView!
    @IBOutlet weak var dynamicCellTitle: UILabel!
}
