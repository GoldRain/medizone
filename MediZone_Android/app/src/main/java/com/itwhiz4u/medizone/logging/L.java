package com.itwhiz4u.medizone.logging;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class L {
    private static final String mTag = "MEDIZONE_LOG";
    public static Boolean DEBUG_MODE = false;

    public static void d(String message) {
        if(DEBUG_MODE)
        Log.d(mTag, message);
    }
    public static void e(String message) {
        if(DEBUG_MODE)
            Log.e(mTag, message);
    }
    public static void t(Context context, String message) {
        if (DEBUG_MODE) Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
