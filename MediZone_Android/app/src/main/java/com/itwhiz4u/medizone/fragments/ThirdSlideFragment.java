package com.itwhiz4u.medizone.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itwhiz4u.medizone.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdSlideFragment extends Fragment {
    public ThirdSlideFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.bg_fram3, container, false);

        return view;
    }

}
