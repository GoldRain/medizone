
package com.itwhiz4u.medizone.model.songs;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("artwork")
    @Expose
    private String artwork;
    @SerializedName("audio_file")
    @Expose
    private String audioFile;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("is_top")
    @Expose
    private Integer isTop;
    @SerializedName("is_new")
    @Expose
    private Integer isNew;
    @SerializedName("top_position")
    @Expose
    private Integer topPosition;
    @SerializedName("new_position")
    @Expose
    private Object newPosition;
    @SerializedName("cat_id")
    @Expose
    private Integer catId;
    @SerializedName("subcat_id")
    @Expose
    private Integer subcatId;
    @SerializedName("status")
    @Expose
    private String status;
    public final static Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            return new Datum(in);
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
    ;

    protected Datum(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.artwork = ((String) in.readValue((String.class.getClassLoader())));
        this.audioFile = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.isTop = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.isNew = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.topPosition = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.newPosition = ((Object) in.readValue((Object.class.getClassLoader())));
        this.catId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.subcatId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Datum() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtwork() {
        return artwork;
    }

    public void setArtwork(String artwork) {
        this.artwork = artwork;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }

    public Integer getIsNew() {
        return isNew;
    }

    public void setIsNew(Integer isNew) {
        this.isNew = isNew;
    }

    public Integer getTopPosition() {
        return topPosition;
    }

    public void setTopPosition(Integer topPosition) {
        this.topPosition = topPosition;
    }

    public Object getNewPosition() {
        return newPosition;
    }

    public void setNewPosition(Object newPosition) {
        this.newPosition = newPosition;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getSubcatId() {
        return subcatId;
    }

    public void setSubcatId(Integer subcatId) {
        this.subcatId = subcatId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(artwork);
        dest.writeValue(audioFile);
        dest.writeValue(duration);
        dest.writeValue(isTop);
        dest.writeValue(isNew);
        dest.writeValue(topPosition);
        dest.writeValue(newPosition);
        dest.writeValue(catId);
        dest.writeValue(subcatId);
        dest.writeValue(status);
    }

    public int describeContents() {
        return  0;
    }

}
