package com.itwhiz4u.medizone.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.activities.MainActivity;
import com.itwhiz4u.medizone.utils.Constants;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by ptyagi on 4/17/17.
 */

/**
 * AlarmReceiver handles the broadcast message and generates Notification
 */
public class AlarmReceiver extends BroadcastReceiver {
    private NotificationManager mNotificationManager;
    @Override
    public void onReceive(Context context, Intent intent1) {
        //Get notification manager to manage/send notifications

        String valueIntent = "Notification";
        //Intent to invoke app when click on notification.
        //In this sample, we want to start/launch this sample app when user clicks on notification
        Intent intentToRepeat = new Intent(context, MainActivity.class);
        intentToRepeat.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intentToRepeat.putExtra(Constants.KEY_GOOGLE_ClOUD_MESSAGING, valueIntent);
        intentToRepeat.putExtra(Constants.KEY_sender_message, intent1.getStringExtra("data"));
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intentToRepeat,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.android_trans)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher))
                .setContentTitle(context.getString(R.string.app_name))
                .setStyle(new android.support.v4.app.NotificationCompat.BigTextStyle()
                        .bigText(intent1.getStringExtra("data")))
                .setContentText(intent1.getStringExtra("data"))
                .setAutoCancel(true)
                .setTicker("ticker");


        mBuilder.setContentIntent(contentIntent);
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(notificationSound);
        mNotificationManager.notify(1, mBuilder.build());

        String[] array = context.getResources().getStringArray(R.array.seven_day_notification);
        String randomStr = array[new Random().nextInt(array.length)];
        int day=2;
        NotificationHelper.cancelAlarmRTC();

        if(intent1.hasExtra("data")){
            if (Arrays.asList(array).contains(intent1.getStringExtra("data"))) {
                NotificationHelper.scheduleRepeatingRTCNotification(context, 7,
                        randomStr);
            }else {
                NotificationHelper.scheduleRepeatingRTCNotification(context, 6,
                        randomStr);
            }
        }

    }
}
