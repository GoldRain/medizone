package com.itwhiz4u.medizone.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.itwhiz4u.medizone.fragments.FristSlideFragment;
import com.itwhiz4u.medizone.fragments.SecondSlideFragment;
import com.itwhiz4u.medizone.fragments.ThirdSlideFragment;


/**
 * Created by sonpham on 7/29/2016.
 */
public class CustomViewPagerAdapter extends FragmentPagerAdapter {
    public CustomViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {
            case 0:
                return new FristSlideFragment();
            case 1:
                return new SecondSlideFragment();
            case 2:
                return new ThirdSlideFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}