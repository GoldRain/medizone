package com.itwhiz4u.medizone.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.activities.MainActivity;
import com.itwhiz4u.medizone.adapter.TopTrackAdapter;
import com.itwhiz4u.medizone.interfaces.IImpostoActionListner;
import com.itwhiz4u.medizone.logging.L;
import com.itwhiz4u.medizone.model.songs.BeenSong;
import com.itwhiz4u.medizone.model.songs.Datum;
import com.itwhiz4u.medizone.network.ServicesClient;
import com.itwhiz4u.medizone.utils.Constants;
import com.itwhiz4u.medizone.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rddigital-006 on 7/12/17.
 */

public class Fragment_TrackList extends Fragment {
    MainActivity mActivity;
    private Unbinder unbinder;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<Datum>  listSongs = new ArrayList<Datum>();;
    AdView mAdView;
    boolean isPagination=true;
    TopTrackAdapter topTrackAdapter;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    int mPageIndex=1, pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager mLinearLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        return;
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setToolBarTitle(getArguments().getString("Title"));
        mActivity.mFragment=this;
        if (mAdView != null) {
            mAdView.resume();
        }
    }
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       // if(view==null) {
            view = inflater.inflate(R.layout.fragment_top_track, container, false);
            mActivity.setupParent(view);
            unbinder = ButterKnife.bind(this, view);
            L.e("Tyep" + getArguments().getInt("Type"));
            topTrackAdapter = new TopTrackAdapter(getActivity(), listSongs, new IImpostoActionListner() {
                @Override
                public void onItemClickListner(int pos, String name, int id) {
                    mActivity.mFragment = new Fragment_PayingSong();
                    Bundle args = new Bundle();
                    args.putString("Title", name);
                    args.putInt("pos", pos);
                    args.putParcelableArrayList("listSongs", listSongs);
                    mActivity.mFragment.setArguments(args);
                    mActivity.showFragment(mActivity.mFragment);
                }
            }, getArguments().getInt("Type"));

            mLinearLayoutManager = new LinearLayoutManager(getContext());
            mLinearLayoutManager.setAutoMeasureEnabled(true);
            recyclerView.setLayoutManager(mLinearLayoutManager);
            recyclerView.setAdapter(topTrackAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            initAds(view);
            if(listSongs.size()==0)
                getSongs();
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    listSongs.clear();
                    mPageIndex = 1;
                    getSongs();
                    //show();
                    Log.d("DEBUG", "onResume -- HomeFragment");
                }
            });

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) //check for scroll down
                    {
                        visibleItemCount = mLinearLayoutManager.getChildCount();
                        totalItemCount = mLinearLayoutManager.getItemCount();
                        pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();

                        if (isPagination) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                L.e("..." + "Last Item Wow !");
                                //Do pagination.. i.e. fetch new data
                                loadmoreData();

                            }
                        }
                    }
                }
            });
       // }
        return view;
    }
    private void loadmoreData() {
        L.d("Load more Data");
        // mPbLoading.setVisibility(View.GONE);
        mPageIndex += 1;
        getSongs();
    }
    private void getSongs() {
        HashMap<String, String> headers =new HashMap<>();
        headers.put("un", Constants.AUTH_EMAIL);
        headers.put("psw",Constants.AUTH_Password);
        ServicesClient.getInstance().makeRequest(headers);

        Call<BeenSong> call=null;
        if(getArguments().getInt("Type")==0){
            isPagination=false;
             call=ServicesClient.getInstance().getServices().
                    getTopSongs();
        }else if( getArguments().getInt("Type")==1){
            isPagination=false;
            call=ServicesClient.getInstance().getServices().
                    getNewSong();
        }else {
            L.e("Type"+ getArguments().getInt("cat_id"));
            L.e("Type"+ getArguments().getInt("SubCatID"));
            call=ServicesClient.getInstance().getServices().
                    getCategorySongs(getArguments().getInt("cat_id"),
                            getArguments().getInt("SubCatID"), mPageIndex);
        }

        showProgrss();
        call.enqueue(new Callback<BeenSong>() {
            @Override
            public void onResponse(Call<BeenSong> call, Response<BeenSong> response) {
                dismisProgrss();
                BeenSong beenSong = response.body();
                if (beenSong != null) {
                    try {
                        if (beenSong.getStatusCode() == 200) {
                            if(beenSong.getData()!=null && beenSong.getData().size()>0){
                                listSongs.addAll(beenSong.getData());
                                topTrackAdapter.notifyDataSetChanged();
                            }else{
                                isPagination=false;
                            }
                        }else {
                            Utils.ShowToast(beenSong.getMessage(),mActivity);
                        }

                    }catch (Exception e){
                        isPagination=false;
                    }
                }
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);

                if(listSongs.size()==0){
                    tvNoData.setVisibility(View.VISIBLE);
                }else {
                    tvNoData.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<BeenSong> call, Throwable t) {
                dismisProgrss();
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void initAds(View view) {
        mAdView = (AdView) view.findViewById(R.id.adView);
        //mAdView.setAdSize(AdSize.BANNER);
      //  mAdView.setAdUnitId(getString(R.string.banner_home_footer));

        AdRequest adRequest = new AdRequest.Builder()
                // Check the LogCat to get your test device ID
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getActivity(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getActivity(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getActivity(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
        unbinder.unbind();
    }


    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    private ProgressDialog pDialog;
    protected void showProgrss() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            // Set progressbar message
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog!=null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
