package com.itwhiz4u.medizone.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.activities.MainActivity;
import com.itwhiz4u.medizone.adapter.CategoryAdapter;
import com.itwhiz4u.medizone.interfaces.IImpostoActionListner;
import com.itwhiz4u.medizone.model.categories.Categories;
import com.itwhiz4u.medizone.model.categories.Datum;
import com.itwhiz4u.medizone.network.ServicesClient;
import com.itwhiz4u.medizone.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rddigital-006 on 7/12/17.
 */

public class Fragment_MainCategory extends Fragment {
    MainActivity mActivity;
    private Unbinder unbinder;
    ArrayList<Datum> listCatagory = new ArrayList<Datum>();;
    AdView mAdView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    CategoryAdapter categoryAdapter;
    View view;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();

        return;
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setToolBarTitle("");
        mActivity.mFragment=this;
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      //  if(view==null) {
            view = inflater.inflate(R.layout.fragment_main_category, container, false);
            mActivity.setupParent(view);
            unbinder = ButterKnife.bind(this, view);
           // listCatagory = new ArrayList<Datum>();
            categoryAdapter = new CategoryAdapter(getActivity(), listCatagory, 1, new IImpostoActionListner() {
            @Override
            public void onItemClickListner(int pos, String titel, int id) {
                if (pos == 1 ) {
                    mActivity.openTopTrack(titel, pos, 0,0);
                }else if (pos == 0) {
                    mActivity.openTopTrack(titel, pos,0,0);
                }else {
                    mActivity.mFragment = new Fragment_SubCategory();
                    Bundle args = new Bundle();
                    args.putString("Title", titel);
                    args.putString("desc", listCatagory.get(pos).getDesc());
                    args.putInt("ID", id);
                    mActivity.mFragment.setArguments(args);
                    mActivity.showFragment(mActivity.mFragment);
                }
            }
        });
        recyclerView.setAdapter(categoryAdapter);
            GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);
            if(listCatagory.size()==0) {
                getCatagories();
            }
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    //  mStoryModels.clear();
                    listCatagory.clear();
                    categoryAdapter.notifyDataSetChanged();


                    getCatagories();
                    //show();
                    Log.d("DEBUG", "onResume -- HomeFragment");
                }
            });

       // }
        initAds(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getCatagories() {
        try {

        HashMap<String, String> headers =new HashMap<>();
        headers.put("un", Constants.AUTH_EMAIL);
        headers.put("psw",Constants.AUTH_Password);
        ServicesClient.getInstance().makeRequest(headers);
        showProgrss();
        Call<Categories> call=ServicesClient.getInstance().getServices().
                getAllCategory();
        call.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {
                dismisProgrss();

                Categories categories = response.body();
                if (categories != null) {
                    try {
                    if (categories.getStatusCode() == 200) {
                        if(categories.getData()!=null && categories.getData().size()>0);{
                            listCatagory.clear();
                            listCatagory.add(new Datum("Top Meditations"));
                            listCatagory.add(new Datum("New Meditations"));
                            listCatagory.addAll(categories.getData());


                        }
                    } else {

                    }
                    }catch (Exception e){

                    }
                }
                categoryAdapter.notifyDataSetChanged();
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {
                dismisProgrss();
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
                categoryAdapter.notifyDataSetChanged();
            }
        });
        }catch (Exception e){

        }
    }

    private ProgressDialog pDialog;
    protected void showProgrss() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            // Set progressbar message
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog!=null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
    private void initAds(View view) {
        mAdView = (AdView) view.findViewById(R.id.adView);
        //mAdView.setAdSize(AdSize.BANNER);
        //  mAdView.setAdUnitId(getString(R.string.banner_home_footer));

        AdRequest adRequest = new AdRequest.Builder()
                // Check the LogCat to get your test device ID
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getActivity(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getActivity(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getActivity(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
        unbinder.unbind();
    }



    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }
}

