package com.itwhiz4u.medizone.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.activities.MainActivity;
import com.itwhiz4u.medizone.activities.RewardedVideoAdActivity;
import com.itwhiz4u.medizone.logging.L;
import com.itwhiz4u.medizone.model.songs.Datum;
import com.itwhiz4u.medizone.utils.Constants;
import com.itwhiz4u.medizone.utils.SharedPrefsUtils;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by rddigital-006 on 7/12/17.
 */

public class Fragment_PayingSong extends Fragment {
    MainActivity mActivity;
    private Unbinder unbinder;
    @BindView(R.id.title)
    TextView title;
    AdView mAdView;

    @BindView(R.id.ivNext) ImageButton ivNext;
    @BindView(R.id.ivPrevious) ImageButton ivPrevious;
    @BindView(R.id.ivPlay) ImageButton ivPlay;
    @BindView(R.id.ivtrack) ImageView ivTrack;
    @BindView(R.id.pb)
    ProgressBar progressBar;


    int currentSongIndex=0, totalSongLength=0;
    ArrayList<Datum> listSongs;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        return;
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setToolBarTitle("Now Playing");
        mActivity.mFragment=this;
        if (mAdView != null) {
            mAdView.resume();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_playing, container, false);
        mActivity.setupParent(view);
        unbinder = ButterKnife.bind(this, view);

        title.setText(getArguments().getString("Title"));
        currentSongIndex=getArguments().getInt("pos");
        listSongs=new ArrayList<>();
        listSongs= getArguments().getParcelableArrayList("listSongs");
        totalSongLength=listSongs.size();
        imgeTrack();

        initAds(view);
        if(totalSongLength==1){
            ivPrevious.setEnabled(false);
            ivPrevious.setAlpha(50);
            ivNext.setEnabled(false);
            ivNext.setAlpha(50);
            ivNext.setClickable(false);
            ivPrevious.setClickable(false);

        }else if(currentSongIndex == 0){
            ivPrevious.setEnabled(false);
            ivPrevious.setAlpha(50);
        }else if(currentSongIndex == totalSongLength-1){
            ivNext.setEnabled(false);
            ivNext.setAlpha(50);

        }

        return view;
    }

    public void imgeTrack(){
        Glide.with(getActivity()).load(Constants.BASE_SONGSImage + listSongs.get(currentSongIndex).getArtwork())
                .into(ivTrack);
        L.e(Constants.BASE_SONGSImage + listSongs.get(currentSongIndex).getArtwork());
    }

    @OnClick(R.id.ivNext)
    public void playNext(){
        if(mActivity.timeCountInMilliSeconds<1000){
            openDialogOnFinishTime();
            return;
        }
        if(totalSongLength > 1 && currentSongIndex != totalSongLength-1 ){
            currentSongIndex=currentSongIndex+1;
            stopPlayer();
            title.setText(listSongs.get(currentSongIndex).getName());
            imgeTrack();
        }

        if(currentSongIndex == totalSongLength-1){
            ivNext.setEnabled(false);
            ivNext.setAlpha(50);
        }
        ivPrevious.setEnabled(true);
        ivPrevious.setAlpha(-1);


    }
    @OnClick(R.id.ivPrevious)
    public void playPrevious(){
        if(mActivity.timeCountInMilliSeconds==0){
            openDialogOnFinishTime();
            return;
        }
        if(totalSongLength>1 && currentSongIndex !=0 ){
            currentSongIndex=currentSongIndex-1;
            stopPlayer();
            title.setText(listSongs.get(currentSongIndex).getName());
            imgeTrack();
        }

        if(currentSongIndex == 0){
            ivPrevious.setEnabled(false);
            ivPrevious.setAlpha(50);
        }

        ivNext.setEnabled(true);
        ivNext.setAlpha(-1);
    }


    private void stopPlayer() {
        try {
        stopCountDownTimer();
        if(mPlayer!=null) {
            if(mPlayer.isPlaying()) {
                mPlayer.stop();
                mPlayer.reset();
                mPlayer.release();
            }
            mPlayer=null;
        }
        ivPlay.setImageResource(R.drawable.ic_play);
        ivPlay.setAlpha(-1);
        ivPlay.setEnabled(true);
        ivPlay.setClickable(true);
        }catch (Exception e){

        }
    }

    MediaPlayer mPlayer;
    @OnClick(R.id.ivPlay)
    public void playMusic(){
        if(mActivity.timeCountInMilliSeconds<1000){
            openDialogOnFinishTime();
            return;
        }
       // progressBar.setVisibility(View.VISIBLE);

        ivPlay.setAlpha(50);
        ivPlay.setEnabled(false);
      //  showProgrss();

        if(timerStatus == TimerStatus.STOPPED){
        // The audio url to play
            String audioUrl =  Constants.BASE_SONGS_File + listSongs.get(currentSongIndex).getAudioFile();
            L.e(audioUrl);

        try{
                    /*
                        void setDataSource (String path)
                            Sets the data source (file-path or http/rtsp URL) to use.

                        Parameters
                            path String : the path of the file, or the http/rtsp URL of the stream you want to play

                        Throws
                            IllegalStateException : if it is called in an invalid state

                                When path refers to a local file, the file may actually be opened by a
                                process other than the calling application. This implies that the
                                pathname should be an absolute path (as any other process runs with
                                unspecified current working directory), and that the pathname should
                                reference a world-readable file. As an alternative, the application
                                could first open the file for reading, and then use the file
                                descriptor form setDataSource(FileDescriptor).

                            IOException
                            IllegalArgumentException
                            SecurityException
                    */

            if(mPlayer==null) {
                // Initialize a new media player instance
                mPlayer = new MediaPlayer();
                // Set the media player audio stream type
                mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                //Try to play music/audio from url
            }
            // Set the audio data source
            mPlayer.setDataSource(audioUrl);

                    /*
                        void prepare ()
                            Prepares the player for playback, synchronously. After setting the
                            datasource and the display surface, you need to either call prepare()
                            or prepareAsync(). For files, it is OK to call prepare(), which blocks
                            until MediaPlayer is ready for playback.

                        Throws
                            IllegalStateException : if it is called in an invalid state
                            IOException
                    */
            // Prepare the media player
            mPlayer.prepare();
            mPlayer.start();
            Toast.makeText(getContext(),"Playing",Toast.LENGTH_SHORT).show();
            startCountDownTimer();
            ivPlay.setAlpha(-1);
            ivPlay.setImageResource(R.drawable.ic_stop);
            ivPlay.setEnabled(true);
          //  progressBar.setVisibility(View.GONE);
            /*mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                    Toast.makeText(getContext(),"Playing",Toast.LENGTH_SHORT).show();
                    startCountDownTimer();
                    ivPlay.setAlpha(-1);
                    ivPlay.setImageResource(R.drawable.ic_stop);
                    ivPlay.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                }
            });
            mPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {

                @Override
                public void onVideoSizeChanged(MediaPlayer mp, int arg1, int arg2) {
                    // TODO Auto-generated method stub
                    // Start playing audio from http url
                    L.e("arg1: "+arg1 +"arg2: "+arg2);

                    // Inform user for audio streaming


                }
            });*/
            //progressBar.setVisibility(View.GONE);
        }catch (IOException e){

            // Catch the exception
            e.printStackTrace();
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }catch (SecurityException e){
            e.printStackTrace();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }finally {
        }

        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
              //  Toast.makeText(getActivity(),"End",Toast.LENGTH_SHORT).show();
                stopPlayer();
            }


        });

            dismisProgrss();
        }else{
            dismisProgrss();
            stopPlayer();
           // progressBar.setVisibility(View.GONE);
        }
    }

    private static final String TAG = "Fragment_PayingSong";
    @OnClick(R.id.autopause)
    public void autopause(){
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_autipause);
        ImageView dialogButton = (ImageView) dialog.findViewById(R.id.Close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ImageView pause = (ImageView) dialog.findViewById(R.id.pause);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void showDialog(Activity activity){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_got_min);
       // Button btnFree=(Button) dialog.findViewById(R.id.btnFree);
        //<font color="#57A700"><b>ADD</b></font> <b>30 MORE</b> MIN - <font color="#57A700"><b>FREE</b> (VIDEO)</font>
     /*   String str1="ADD ";
        String str2="30 More ";
        String str3="Min ";
        String str4=" Free ";
        String str5="(Video)";
        TextDecorator
                .decorate(btnFree, str1+str2+str3+"-"+str4+str5)
                .setTextColor(R.color.green, str1, str4,str5)
                .setTextStyle(Typeface.BOLD, str2)
                .setRelativeSize(1,str3,str5)
                .setTextColor(android.R.color.white, str2,str3)
                .build();*/

        ImageView dialogButton = (ImageView) dialog.findViewById(R.id.Close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    private ProgressDialog pDialog;
    protected void showProgrss() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            // Set progressbar message
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog!=null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    private void initAds(View view) {
        mAdView = (AdView) view.findViewById(R.id.adView);
      //  mAdView.setAdSize(AdSize.BANNER);
      //  mAdView.setAdUnitId(getString(R.string.banner_home_footer));

        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                //.addTestDevice("C04B1BFFB0774708339BC273F8A43708")
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getActivity(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getActivity(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getActivity(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }

    @Override
    public void onDestroy() {
       stopPlayer();
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
        unbinder.unbind();
    }

/*
    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
*/


    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }




    private CountDownTimer countDownTimer;
    public TimerStatus timerStatus = TimerStatus.STOPPED;

   

    public enum TimerStatus {
        STARTED,
        STOPPED
    }
    /**
     * method to stop count down timer
     */
    public void stopCountDownTimer() {
        if(timerStatus != TimerStatus.STARTED)
            return;
        if(countDownTimer!=null )
            countDownTimer.cancel();

        timerStatus = TimerStatus.STOPPED;
    }
    public void startCountDownTimer() {
        timerStatus = TimerStatus.STARTED;
        countDownTimer = new CountDownTimer(mActivity.timeCountInMilliSeconds, 1000) {
            @Override
                public void onTick(long millisUntilFinished) {
                mActivity.time.setText(mActivity.hmsTimeFormatter(millisUntilFinished));
                mActivity.timeCountInMilliSeconds=millisUntilFinished;
                SharedPrefsUtils.SavePrefSelected_SECONDS(getActivity(),mActivity.timeCountInMilliSeconds);

            }

            @Override
            public void onFinish() {
                openDialogOnFinishTime();
                stopPlayer();
                mActivity.timeCountInMilliSeconds=0;
                mActivity.time.setText(mActivity.hmsTimeFormatter(mActivity.timeCountInMilliSeconds));
            }

        }.start();

    }

    private void openDialogOnFinishTime() {
        if(mPlayer!=null)
            mPlayer.stop();
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);

        // retrieve display dimensions
        Rect displayRectangle = new Rect();
        Window window = mActivity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

// inflate and adjust layout
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_time_out, null);
        layout.setMinimumWidth((int)(displayRectangle.width() * 0.9f));

        dialog.setContentView(layout);
        ImageView dialogButton = (ImageView) layout.findViewById(R.id.Close);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        layout.findViewById(R.id.btnVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivityForResult(new Intent(mActivity, RewardedVideoAdActivity.class),1001);
            }
        });
        dialog.show();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
