package com.itwhiz4u.medizone.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.interfaces.IImpostoActionListner;
import com.itwhiz4u.medizone.model.categories.Datum;
import com.itwhiz4u.medizone.utils.Constants;

import java.util.ArrayList;

/**
 * Created by 3rddigital-001 on 12/07/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private Context context;
    private IImpostoActionListner listener;
    ArrayList<Datum> mCategoryItem;
    public int TYPE;

    public CategoryAdapter(Context context, ArrayList<Datum> CategoryItem,int type, IImpostoActionListner listener) {
        this.mCategoryItem = CategoryItem;
        this.context = context;
        this.listener =listener;
        this.TYPE=type;
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gridview_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        final Datum clientItem=mCategoryItem.get(i);

        viewHolder.itemTitle.setLines(1);
        if(TYPE==1) {
            if(i==0 || i==1) {
                viewHolder.itemTitle.setText(clientItem.getName().replace(" ", "\n"));
                viewHolder.itemTitle.setLines(2);
            }else {
                viewHolder.itemTitle.setText(clientItem.getName());
            }

            if(!TextUtils.isEmpty(clientItem.getImage())) {
                Glide.with(context).load(Constants.BASE_IMAGE_Catagories + clientItem.getImage()).
                        into(viewHolder.image);
                viewHolder.itemImageback.setVisibility(View.INVISIBLE);
                viewHolder.image.setVisibility(View.VISIBLE);
            }else {
                viewHolder.itemImageback.setVisibility(View.VISIBLE);
                viewHolder.image.setVisibility(View.GONE);
            }
        }else {
            viewHolder.itemTitle.setText(clientItem.getName());

          if(!TextUtils.isEmpty(clientItem.getImage()))
                Glide.with(context).load(Constants.BASE_IMAGE_SubCatagories +
                        clientItem.getImage()).into(viewHolder.image);

            viewHolder.itemImageback.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                viewHolder.itemTitle.setTextAppearance(R.style.font_OpenSansRegular);
            }else {
                viewHolder.itemTitle.setTextAppearance(context,R.style.font_OpenSansRegular);
            }
          //  viewHolder.itemTitle.setPadding(0,7,0,7);

        }
        viewHolder.linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP){
                    listener.onItemClickListner(i, clientItem.getName(), clientItem.getId());
                    // Do what you want
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCategoryItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView itemTitle;
        public ImageView image,itemImageback;
        public LinearLayout linearLayout;
        public ViewHolder(View view) {
            super(view);
            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.image = (ImageView) view.findViewById(R.id.itemImage);
            this.itemImageback = (ImageView) view.findViewById(R.id.itemImageback);
            this.linearLayout=(LinearLayout) view.findViewById(R.id.relativeLayout1);
        }
    }

}
