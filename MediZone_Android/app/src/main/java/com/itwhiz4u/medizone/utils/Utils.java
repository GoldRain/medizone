package com.itwhiz4u.medizone.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.interfaces.IDialogAction;
import com.itwhiz4u.medizone.logging.L;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sonpham on 1/25/2016.
 */
public class Utils {

    /**
     * Determine if the device is a tablet (i.e. it has a large screen).
     *
     * @param context The calling context.
     */
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static DisplayMetrics getDeviceMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }

    /* Hide soft keyboard when touch to layout out edit text*/
    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            if (activity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
            if (activity.getWindow() != null)
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }

    }

    public static void hideSoftKeyboard(View view, Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        if (activity.getWindow() != null)
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //region Check Internet State
    /* Check status Internet connecting*/
    public static boolean checkIntenetStatus(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    //endregion
    public static String getVideoID(String s) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
        String id = "";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(s); //url is youtube url for which you want to extract the id.
        if (matcher.find()) {
            id = matcher.group();
        }
        return "http://i3.ytimg.com/vi/" + id + "/hqdefault.jpg";
    }

    /**
     * @param url
     * @return
     */
    public static String getidByURL(String url) {

        String[] separated = url.split("/");
        return separated[4];
    }

    public static String getidByURL1(String url) {
        String[] separated = url.split("/");
        return separated[4];
    }

    public static boolean isVideoYoute(String url) {

        if (url != null && url.trim().length() > 0 && url.startsWith("http")) {
            String expression = "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*";
            CharSequence input = url;
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(input);
            if (!matcher.matches()) {
                return false;
            }
        }
        return true;
    }

    public static String getVideoYoutubeID(String url) {
        String video_id = "";
        if (url != null && url.trim().length() > 0 && (url.startsWith("http") || url.startsWith("https"))) {
            String expression = "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*";
            CharSequence input = url;
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(input);
            if (matcher.matches()) {
                String groupIndex1 = matcher.group(7);
                if (groupIndex1 != null && groupIndex1.length() == 11) video_id = groupIndex1;
                groupIndex1 = url.subSequence(url.lastIndexOf("/") + 1, url.length()).toString();
                if (groupIndex1 != null && groupIndex1.length() == 11) video_id = groupIndex1;
            }
        }
        return video_id;
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        String tmp = "";

        if (image != null) {
            try {
                ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
                image.compress(compressFormat, quality, byteArrayOS);
                //System.gc();
                tmp = Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                L.d("Utils:" + e);
            }
        } else {
            L.d("Utils: null");
        }
        return tmp;
    }

    public static Bitmap decodeBase64(String input) {
        Bitmap tmp = null;
        byte[] decodedBytes = Base64.decode(input, 0);
        try {
            tmp = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            L.d("Out of memory error catched");
        }
        return tmp;
    }

    public static String getHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return ("KeyHash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            return e.toString();
        }
        return "";
    }

    public static String isLogin(Context context) {
       /* UserLoginBean userLoginBean = SharedPrefsUtils.getSavedObjectFromPreference(context, UserLoginBean.class.getName(), UserLoginBean.class);
        if (userLoginBean != null) {
            return userLoginBean.getData().get(0).getToken();
        }*/
        return null;
    }
  /* public static UserLoginBean getLoginUser(Context context) {
        UserLoginBean userLoginBean = SharedPrefsUtils.getSavedObjectFromPreference(context, UserLoginBean.class.getName(), UserLoginBean.class);

        return userLoginBean;
    }*/
    //region SHARED PREFERENCES UTILS

    public static void clearSharedPreferences(Context context, String filename) {
        SharedPreferences pre = context.getSharedPreferences(filename, context.MODE_PRIVATE);
        pre.edit().clear().commit();
    }

    public static Dialog setDialog(Context context, View view) {
        Dialog dialog_post_type = new Dialog(context);
        dialog_post_type.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_post_type.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_post_type.setCancelable(true);
        dialog_post_type.setContentView(view);
        return dialog_post_type;
    }

    //endregion
    public static void showDialog(Activity context, String btnok, String btnCanCel, int icon, String title, String message,
                                  final IDialogAction iDialogAction) {
     /*   View layout = context.getLayoutInflater().inflate(R.layout.dialog_ok, null);
        ImageView ivIcon = (ImageView) layout.findViewById(R.id.ivIcon);
        TextView tvTitel = (TextView) layout.findViewById(R.id.tv_title);
        TextView tvMessge = (TextView) layout.findViewById(R.id.tvMessage);
        tvTitel.setText(title);
        tvMessge.setText(message);
        final Dialog dialog = setDialog(context, layout);

        Button btnOk = (Button) layout.findViewById(R.id.btnOk);

        if (btnOk != null)
            btnOk.setText(btnok);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                if (iDialogAction != null)
                    iDialogAction.onOkPress();
            }
        });
        Button btnCancel = (Button) layout.findViewById(R.id.btnCancel);
        if (btnCanCel != null)
            btnCancel.setText(btnCanCel);
        else
            btnCancel.setVisibility(View.GONE);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                if (iDialogAction != null)
                    iDialogAction.onCancelPress();
            }
        });
        dialog.show();*/
    }


    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+");

    public static boolean checkUserName(String username) {
        return USERNAME_PATTERN.matcher(username).matches();
    }

    public static boolean isValidName(String name) {
        return NAME_PATTERN.matcher(name).matches();
    }

    private static final Pattern USERNAME_PATTERN = Pattern.compile("^[A-Za-z0-9_-]{4,16}$");
    private static final Pattern NAME_PATTERN = Pattern.compile("^[A-Za-z0-9 _-]");

    /**
     * @param s
     * @param maxLength
     * @return SubString
     */
    public static final String getSafeSubstring(String s, int maxLength) {
        if (!TextUtils.isEmpty(s)) {
            if (s.length() >= maxLength) {
                return s.substring(0, maxLength);
            }
        }
        return s;
    }


    public static String timeAgoNotification1(String date) {
        Date tempNow = covertDateToUTCFormat(getCurrentDateTimeForPush(null), "yyyy-MM-dd'T'HH:mm:ss.sssZ");
        Date tempDate = covertDateToUTCFormat(date.replaceAll("Z$", "+0000"), "yyyy-MM-dd HH:mm:ss");
        if (tempDate != null && tempNow != null) {
            double deltaSeconds = ((tempNow.getTime() - tempDate.getTime()) / TimeUnit.SECONDS.toMillis(1));
            double deltaMinutes = deltaSeconds / 60.0f;

            int minutes;

            if (deltaSeconds < 5) {
                return "Just now";
            } else if (deltaSeconds < 60) {
                return String.format(Locale.getDefault(), "%d seconds ago", (int) Math.floor(deltaSeconds));
            } else if (deltaSeconds < 120) {
                return "A minute ago";
            } else if (deltaMinutes < 60) {
                return String.format(Locale.getDefault(), "%d minutes ago", (int) Math.floor(deltaMinutes));
            } else if (deltaMinutes < 120) {
                return "An hour ago";
            } else if (deltaMinutes < (24 * 60)) {
                minutes = (int) Math.floor(deltaMinutes / 60);
                return String.format(Locale.getDefault(), "%d hours ago", minutes);
            } else if (deltaMinutes < (24 * 60 * 2)) {
                return "Yesterday";
            } else if (deltaMinutes < (24 * 60 * 7)) {
                minutes = (int) Math.floor(deltaMinutes / (60 * 24));
                return String.format(Locale.getDefault(), "%d days ago", minutes);
            } else if (deltaMinutes < (24 * 60 * 14)) {
                return "Last week";
            } else if (deltaMinutes < (24 * 60 * 31)) {
                minutes = (int) Math.floor(deltaMinutes / (60 * 24 * 7));
                return String.format(Locale.getDefault(), "%d weeks ago", minutes);
            } else if (deltaMinutes < (24 * 60 * 61)) {
                return "Last month";
            } else if (deltaMinutes < (24 * 60 * 365.25)) {
                minutes = (int) Math.floor(deltaMinutes / (60 * 24 * 30));
                return String.format(Locale.getDefault(), "%d months ago", minutes);
            } else if (deltaMinutes < (24 * 60 * 731)) {
                return "Last year";
            }

            minutes = (int) Math.floor(deltaMinutes / (60 * 24 * 365));
            return String.format(Locale.getDefault(), "%d years ago", minutes);
        } else
            return "";
    }

    public static String covertDateFormat(String date_time, String from_format, String to_formate) {
        String convertedFormat = null;
        Locale locale = Locale.getDefault();
        SimpleDateFormat from_SimpleDateFormat = new SimpleDateFormat(from_format, locale);
        SimpleDateFormat to_SimpleDateFormat = new SimpleDateFormat(to_formate, locale);
        // format_before.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date actualDate = from_SimpleDateFormat.parse(date_time);
            convertedFormat = to_SimpleDateFormat.format(actualDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return convertedFormat;
    }

    public static Date covertDateToUTCFormat(String date_time, String current_format) {
        Date actualDate = null;
        Locale locale = Locale.getDefault();
        SimpleDateFormat format_before = new SimpleDateFormat(current_format, locale);
        format_before.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            actualDate = format_before.parse(date_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return actualDate;
    }

    public static String getCurrentDateTimeForPush(String patern) {
        try {
            Locale l = Locale.getDefault();
            if (patern == null)
                patern = "yyyy-MM-dd'T'HH:mm:ss.sssZ";
            SimpleDateFormat sdf = new SimpleDateFormat(patern, l);
            // sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            return sdf.format(Calendar.getInstance().getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "0000-00-00T00:00:00.000Z";
        }
    }

    static AlertDialog alertDialog;

    public static boolean checkInternetConnection(final boolean isFinish, final Activity activity) {
        if (!checkIntenetStatus(activity)) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder.setTitle(activity.getResources().getString(R.string.main_activity_title_alter))
                    .setMessage(activity.getResources().getString(R.string.main_activity_title_alter_Message))
                    .setCancelable(false).
                    setPositiveButton(activity.getResources().getString(R.string.lbl_setting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                  /*  if(isFinish)
                        activity.finish();*/
                            Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                            // intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                            activity.startActivity(intent);

                        }
                    });

            //  if (!alertDialog.isShowing()){
            // create alert dialog
            alertDialog = alertDialogBuilder.create();
            // show it

            alertDialog.show();
            // }
            return false;
        } else {
            return true;
        }
    }

    public static void ShowToast(String message, Context activity) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void showDialog(String Titel, String message, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(Titel).setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.lbl_oky, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showDialog(String message, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.lbl_oky) , new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }




    public static String converMinute(int seconds) {
        if (seconds < 60) {
            return seconds + " second";
        } else {
            int minute = seconds / 60;
            return minute + " minute";
        }
    }



    public static String getDateCurrentTimeZone(long timestamp, String pattern) {
       /* try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        }catch (Exception e) {
        }*/
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        String dateString = formatter.format(new Date(timestamp));
        return dateString;
    }

    public static String urlEncodeUTF8(String s) {
        try {
            return new String(s.getBytes("UTF-8"));//URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static HashMap<String, String> urlEncodeUTF8(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();
        HashMap<String, String> mapR = new HashMap<>();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            try {
                mapR.put(entry.getKey().toString(), URLEncoder.encode(entry.getValue().toString(),"UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
        return mapR;
    }


    public static String strDecodeUTF8(String s) {
        try {
            s = URLDecoder.decode(s, "UTF-8");//
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }
    public static String strDecodeUTF81(String s) {
        try {
            s = new String(s.getBytes("ISO-8859-1"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }



}
