package com.itwhiz4u.medizone.utils;

/**
 * Created by rddigital-006 on 21/9/17.
 */

public interface Constants {
   String STRING_EMPTY = "";

   String BASE="http://dev.3rddigital.com/medizone/";
   String BASE_IMAGE_Catagories =BASE+ "categoryimg/";
   String BASE_IMAGE_SubCatagories =BASE+ "subcategoryimg/";
   String BASE_URL = BASE+"api/";
   String BASE_SONGS_File=BASE+"audiofiles/";
   String BASE_SONGSImage=BASE+"artwork/";

   String CONTACT_US_EMAIL="info@medizone.co";
   String REQUEST_EMAIL="info@medizone.co";
   String SUBJECT_REQUEST="Request";

   String AUTH_EMAIL="admin@medizone.com";
   String AUTH_Password="admin@123";

   String KEY_GOOGLE_ClOUD_MESSAGING = "key1";
   String KEY_sender_message = "message";

   /* for songs-> baseurl/artwork/imgname
for audio-> baseurl/audiofiles/filename
for category-> baseurl/categoryimg/imgname
for subcategory-> baseurl/subcategoryimg/imgname*/
}
