package com.itwhiz4u.medizone.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.itwhiz4u.medizone.R;


/**
 * Created by sonpham on 7/28/2016.
 */
public class SharedPrefsUtils {
       public static void saveObjectToSharedPreference(Context context, String prefTagName, Object object) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
            final Gson gson = new Gson();
            String serializedObject = gson.toJson(object);
            sharedPreferencesEditor.putString(prefTagName, serializedObject);
            sharedPreferencesEditor.apply();
        }

    public static <GenericClass> GenericClass getSavedObjectFromPreference(Context context, String prefTagName, Class<GenericClass> classType) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        if (sharedPreferences.contains(prefTagName)) {
            final Gson gson = new Gson();
            return gson.fromJson(sharedPreferences.getString(prefTagName, ""), classType);
        }
        return null;
    }

    public static void clearSharedPreference(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.clear().apply();
        sharedPreferencesEditor.commit();
    }
    public static void SavePrefProductBuy(Context context, boolean valve)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putBoolean("ProductBuy", valve);
        sharedPreferencesEditor.apply();
    }
    public static Boolean getPrefProductBuy(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        return sharedPreferences.getBoolean("ProductBuy",false);
    }

    public static void SavePrefSelected_SECONDS(Context context, long valve)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putLong("DEFAULT_SECONDS", valve);
        sharedPreferencesEditor.apply();
    }
    public static long getPrefSelected_SECONDS(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        return sharedPreferences.getLong("DEFAULT_SECONDS",0);
    }

    public static void SavePref(Context context, String tagName, String valve)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(tagName, valve);
        sharedPreferencesEditor.apply();
    }
    public static void SaveBoolenPref(Context context, String tagName, boolean valve)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putBoolean(tagName, valve);
        sharedPreferencesEditor.apply();
    }
    public static boolean getBoolenPref(Context context, String tagName)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        return sharedPreferences.getBoolean(tagName,false);
    }
    public static String getPref(Context context, String tagName)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.pref_name), 0);
        return sharedPreferences.getString(tagName,"");
    }
}
