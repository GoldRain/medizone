package com.itwhiz4u.medizone.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.activities.MainActivity;
import com.itwhiz4u.medizone.adapter.CategoryAdapter;
import com.itwhiz4u.medizone.interfaces.IImpostoActionListner;
import com.itwhiz4u.medizone.model.categories.Categories;
import com.itwhiz4u.medizone.model.categories.Datum;
import com.itwhiz4u.medizone.network.ServicesClient;
import com.itwhiz4u.medizone.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rddigital-006 on 7/12/17.
 */

public class Fragment_SubCategory extends Fragment {
    MainActivity mActivity;
    private Unbinder unbinder;
    ArrayList<Datum> listCatagory = new ArrayList<Datum>();;
    
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    CategoryAdapter categoryAdapter;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        return;
    }


    @Override
    public void onResume() {
        super.onResume();
        mActivity.setToolBarTitle(getArguments().getString("Title"));
        mActivity.mFragment=this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_category, container, false);
        mActivity.setupParent(view);
        unbinder = ButterKnife.bind(this, view);

        categoryAdapter=new CategoryAdapter(getActivity(), listCatagory,2, new IImpostoActionListner() {
            @Override
            public void onItemClickListner(int pos, String type, int id) {
                mActivity.openTopTrack(type, 3, getArguments().getInt("ID"),id );
            }
        });
        recyclerView.setAdapter(categoryAdapter);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        if(listCatagory.size()==0)
            getSubCatagories(getArguments().getInt("ID"));

        try {
            title.setText(getArguments().getString("desc"));
        }catch (Exception e){

        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //  mStoryModels.clear();
                getSubCatagories(getArguments().getInt("ID"));
                //show();
            }
        });
        return view;
    }


    private void getSubCatagories(int cid) {
        HashMap<String, String> headers =new HashMap<>();
        headers.put("un", Constants.AUTH_EMAIL);
        headers.put("psw",Constants.AUTH_Password);
        ServicesClient.getInstance().makeRequest(headers);
        showProgrss();
        Call<Categories> call=ServicesClient.getInstance().getServices().
                getAllSubCategory(""+cid);
        call.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {
                dismisProgrss();
                Categories categories = response.body();
                if (categories != null) {
                    if (categories.getStatusCode() == 200) {
                        try {
                        if(categories.getData()!=null && categories.getData().size()>0);{
                            listCatagory.clear();
                            listCatagory.addAll(categories.getData());
                            categoryAdapter.notifyDataSetChanged();
                        }
                        }catch (Exception e){

                        }
                    } else {

                    }

                    if(listCatagory.size()==0){
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText(categories.getMessage());
                    }else {
                        tvNoData.setVisibility(View.GONE);
                        tvNoData.setText("");
                    }
                }


                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {
                dismisProgrss();
                if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                    swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }

    private ProgressDialog pDialog;
    protected void showProgrss() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(getActivity());
            // Set progressbar title
            // Set progressbar message
            pDialog.setMessage("Loading...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }
        if (!pDialog.isShowing()) {
            // Show progressbar
            pDialog.show();
        }
    }

    protected void dismisProgrss() {
        if (pDialog!=null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
