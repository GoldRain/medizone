package com.itwhiz4u.medizone.network;


import com.itwhiz4u.medizone.model.categories.Categories;
import com.itwhiz4u.medizone.model.songs.BeenSong;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by toanho on 3/4/2016.
 */
public interface IWebServices {

    /*name:demo
email:test1@demo.com
password:123demo
country_name:India
zipcode:123456
contact_no:1235778881
video_secs:120
language:english
role_id:2
city:Ahmedabad*/

    @GET(ServicesClient.API_GetAllCategory)
  Call<Categories> getAllCategory();

    @FormUrlEncoded
    @POST(ServicesClient.API_GetAllSubCategory)
    Call<Categories> getAllSubCategory(@Field("cat_id") String cat_id);

    @GET(ServicesClient.API_GetTopSongs)
    Call<BeenSong> getTopSongs();

    @GET(ServicesClient.API_GetNewSongs)
    Call<BeenSong> getNewSong();



  @FormUrlEncoded
  @POST(ServicesClient.API_GetSongs)
  Call<BeenSong> getCategorySongs(@Field("cat_id") int cat_id,
                                  @Field("sub_cat_id") int subcat_id,
                                  @Field("page") int page);

  @FormUrlEncoded
  @POST("GetDeviceToken")
  Call<Categories> syncToken(@Field("token") String token,
                             @Field("platform") String platform);
/*
    @FormUrlEncoded
    @POST(ServicesClient.API_GetOrigenEstado)
    Call<EstadoAndOrigen> getEstdo(@Field("token") String Token);

    @FormUrlEncoded
    @POST(ServicesClient.API_LOGIN)
    Call<UserLoginBean> apilogin(@Field("username") String email,
                                 @Field("password") String password,
                                 @Field("device_token") String device_token,
                                 @Field("platform") String platform);

  *//*  @Headers({
            "Authorization", "admin admin@123"
    })*//*
    @FormUrlEncoded
    @POST(ServicesClient.API_LOGIN__With_Facebook)
    Call<UserLoginBean> apiloginWithFacebook(@Field("email") String email,
                                             @Field("fb_profile_pic_url") String fb_profile_pic_url,
                                             @Field("device_token") String device_token,
                                             @Field("platform") String platform);

    @FormUrlEncoded
    @POST(ServicesClient.API_SIGNUP)
    Call<UserLoginBean> apiSignUP(@Field("username") String username,
                                  @Field("email") String email,
                                  @Field("password") String password,
                                  @Field("type") String type);


    @FormUrlEncoded
    @POST(ServicesClient.API_LOGOUT)
    Call<UserLoginBean> apiLogout(@Field("token") String token, @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST(ServicesClient.API_Delete_lead)
    Call<UserLoginBean> apiDelete_lead(@Field("token") String token, @Field("lead_id") String lead_id);

    @Multipart
    @POST(ServicesClient.API_Create_Lead)
    Call<LeadBean> Create_Lead(@Part("token") RequestBody Token,
                               @Part("direcciongps") RequestBody direcciongps,
                               @Part("RFC") RequestBody RFC,
                               @Part("calle") RequestBody calle,
                               @Part("exterior") RequestBody exterior,
                               @Part("interior") RequestBody interior,
                               @Part("delegacion") RequestBody delegacion,
                               @Part("colonia") RequestBody colonia,
                               @Part("municipio") RequestBody municipio,
                               @Part("estado") RequestBody estado,
                               @Part("origen") RequestBody origen,
                               @Part("cp") RequestBody cp,
                               @Part("lada") RequestBody lada,
                               @Part("website") RequestBody website,
                               @Part("razonsocial") RequestBody razonsocial,
                               @Part("telefono") RequestBody telefono,
                               @Part("gpslatitude") RequestBody gpslatitude,
                               @Part("gpslongitude") RequestBody gpslongitude,
                               @Part MultipartBody.Part file);

    @Multipart
    @POST(ServicesClient.API_Create_Lead)
    Call<LeadBean> Create_LeadNew(@Part("token") RequestBody Token,
                                  @Part("direcciongps") RequestBody direcciongps,
                                  @Part("nombre_comercial") RequestBody nombre_comercial,
                                  @Part("razonsocial") RequestBody razonsocial,
                                  @Part("RFC") RequestBody RFC,
                                  @Part("website") RequestBody website,
                                  @Part("telefono") RequestBody telefono,
                                  @Part("origen") RequestBody origen,
                                  @Part("gpslatitude") RequestBody gpslatitude,
                                  @Part("gpslongitude") RequestBody gpslongitude,
                                  @Part MultipartBody.Part file);
    @FormUrlEncoded
    @POST(ServicesClient.API_GetAllLeads)
    Call<LeadBean> apiLeads(@Field("page") int mPageIndex);

    @FormUrlEncoded
    @POST(ServicesClient.API_GetUserLeads)
    Call<LeadBean> apiMyLeads(@Field("token") String Token,
                              @Field("page") int mPageIndex);*/
}
