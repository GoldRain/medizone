package com.itwhiz4u.medizone;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.ConnectionBuddyConfiguration;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by rddigital-006 on 5/12/17.
 */

public class MyApp extends Application{

    private static Context myApp;
    private static MyApp mInstance;

    public static Context getAppContext() {
        return myApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        myApp=getApplicationContext();
        setFontDefault();
        MobileAds.initialize(this, getString(R.string.admob_app_id));

        ConnectionBuddyConfiguration networkInspectorConfiguration = new ConnectionBuddyConfiguration.Builder(this).build();
        ConnectionBuddy.getInstance().init(networkInspectorConfiguration);
    }
    public void setFontDefault() {
        String path = "OpenSans-Semibold.ttf";
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(path)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

}
