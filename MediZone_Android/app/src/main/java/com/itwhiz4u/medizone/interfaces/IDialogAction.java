package com.itwhiz4u.medizone.interfaces;

/**
 * Created by rddigital-006 on 12/12/17.
 */

public interface IDialogAction {
    void onOkPress();
    void onCancelPress();
}
