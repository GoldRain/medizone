package com.itwhiz4u.medizone.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.logging.L;
import com.itwhiz4u.medizone.utils.Utils;
import com.zplesac.connectionbuddy.ConnectionBuddy;
import com.zplesac.connectionbuddy.interfaces.ConnectivityChangeListener;
import com.zplesac.connectionbuddy.models.ConnectivityEvent;
import com.zplesac.connectionbuddy.models.ConnectivityState;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by rddigital-006 on 22/9/17.
 */

public abstract class BaseActivity extends AppCompatActivity implements ConnectivityChangeListener {

    private static final String TAG = "BaseActivity";
    AlertDialog alertDialog;

    public static String NOTIFICATION_SERVICES_ID = "";

    @Override
    protected void onResume() {
        super.onResume();
        L.e(TAG+" onResume");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
       super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        L.e(TAG+" onCreate");
        if(savedInstanceState != null){
            ConnectionBuddy.getInstance().clearNetworkCache(savedInstanceState);
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getResources().getString(R.string.main_activity_title_alter))
                .setMessage(getResources().getString(R.string.main_activity_title_alter_Message))
                .setCancelable(false).
                setPositiveButton(getResources().getString(R.string.lbl_setting), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                  /*  if(isFinish)
                        activity.finish();*/
                        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        // intent.setClassName("com.android.phone", "com.android.phone.NetworkSetting");
                        startActivity(intent);


                    }
                });
        alertDialog = alertDialogBuilder.create();
    /*    if (!Utils.checkInternetConnection(true,this))
            return;

        IsToken=Utils.isLogin(getApplicationContext());*/

    }


    @Override
    protected void onPause() {
        super.onPause();
        L.e(TAG+" onPause");
        //Utils.hideSoftKeyboard(this);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        ConnectionBuddy.getInstance().notifyConnectionChange(ConnectionBuddy.getInstance().hasNetworkConnection(),this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ConnectionBuddy.getInstance().registerForConnectivityEvents(this, this);
    }
    @Override
    protected void onStop() {
        super.onStop();
        ConnectionBuddy.getInstance().unregisterFromConnectivityEvents(this);
    }

    @Override
    public void onConnectionChange(ConnectivityEvent event) {
        if(event.getState().getValue() == ConnectivityState.CONNECTED){
            // device has active internet connection
            // showSnack(true);
            // showFragment(mFragment);
            if(alertDialog!=null && alertDialog.isShowing())
                alertDialog.dismiss();
        }
        else{
            // there is no active internet connection on this device
            // showSnack(false);
            if(alertDialog!=null && !alertDialog.isShowing())
                alertDialog.show();
        }
    }
    public void hideKeyboardOnOutsideTouch(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideSoftKeyboard(BaseActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                hideKeyboardOnOutsideTouch(innerView);
            }
        }
    }
}
