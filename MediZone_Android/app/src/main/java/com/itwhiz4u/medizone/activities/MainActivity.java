package com.itwhiz4u.medizone.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.google.firebase.iid.FirebaseInstanceId;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.fragments.Fragment_MainCategory;
import com.itwhiz4u.medizone.fragments.Fragment_TimeView;
import com.itwhiz4u.medizone.fragments.Fragment_TrackList;
import com.itwhiz4u.medizone.logging.L;
import com.itwhiz4u.medizone.model.categories.Categories;
import com.itwhiz4u.medizone.network.ServicesClient;
import com.itwhiz4u.medizone.service.NotificationHelper;
import com.itwhiz4u.medizone.utils.Constants;
import com.itwhiz4u.medizone.utils.SharedPrefsUtils;
import com.itwhiz4u.medizone.utils.Utils;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    public static final String LICENSE_KEY = null;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.txtSubTitle)
    TextView txtSubTitle;
    @BindView(R.id.ivBack)
    ImageButton ivBack;
    @BindView(R.id.ivSetting)
    ImageButton ivSetting;
    @BindView(R.id.time)
     public TextView time;
    public FragmentManager fragmentManager;
    public Fragment mFragment;
    public long timeCountInMilliSeconds=0;

    public boolean readyToPurchase=false;

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.hasExtra(Constants.KEY_GOOGLE_ClOUD_MESSAGING)) {
            Utils.showDialog(intent.getStringExtra(Constants.KEY_sender_message),this);
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        ButterKnife.bind(this);
        NotificationHelper.cancelAlarmRTC();
        String[] array = getResources().getStringArray(R.array.one_day_notification);
        String randomStr = array[new Random().nextInt(array.length)];
        NotificationHelper.scheduleRepeatingRTCNotification(getApplicationContext(),1,
               randomStr);

        timeCountInMilliSeconds = SharedPrefsUtils.getPrefSelected_SECONDS(this);
        if(timeCountInMilliSeconds ==0 ) {
            timeCountInMilliSeconds = 1 * 60000;
            SharedPrefsUtils.SavePrefSelected_SECONDS(this,timeCountInMilliSeconds) ;
        }
        time.setText(hmsTimeFormatter(timeCountInMilliSeconds));
        Utils.hideSoftKeyboard(this);
        fragmentManager = getSupportFragmentManager();
        initDrawerLayout();
        mFragment = new Fragment_MainCategory();
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        showFragment(mFragment);
        setUpInApp();

        if(TextUtils.isEmpty(NOTIFICATION_SERVICES_ID)) {
            NOTIFICATION_SERVICES_ID = FirebaseInstanceId.getInstance().getToken();
            L.e("ToKEN=> " + NOTIFICATION_SERVICES_ID);
        }

        if (getIntent() != null && getIntent().hasExtra(Constants.KEY_GOOGLE_ClOUD_MESSAGING)) {
            Utils.showDialog(getIntent().getStringExtra(Constants.KEY_sender_message),this);
        }

        syncToken();
    }

    @OnClick(R.id.lnNotification)
    public void notification() {
        HideMenuDrawer();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.lbl_Notification))
                .setMessage("Do you want to open notification settings?")
                .setCancelable(false).
                setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent();
                        if(android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1){
                            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                            intent.putExtra("android.provider.extra.APP_PACKAGE", getPackageName());
                        }else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                            intent.putExtra("app_package", getPackageName());
                            intent.putExtra("app_uid", getApplicationInfo().uid);
                        }else {
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + getPackageName()));
                        }
                        startActivity(intent);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        //  if (!alertDialog.isShowing()){
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        // show it

        alertDialog.show();
    }

    @OnClick(R.id.lnRestorePurchase)
    public void RestorePurchase() {
        HideMenuDrawer();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.lbl_RestorePurchase))
                .setMessage("Do you want restore your purchase?")
                .setCancelable(false).
                setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        //  if (!alertDialog.isShowing()){
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        // show it

        alertDialog.show();
    }

    @OnClick(R.id.lnRequest)
    public void emailRequest() {
        HideMenuDrawer();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.lbl_Request))
                .setMessage("Do you want to email request?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendEmail(Constants.REQUEST_EMAIL,Constants.SUBJECT_REQUEST);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        //  if (!alertDialog.isShowing()){
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        // show it

        alertDialog.show();
    }

    @OnClick(R.id.lnContactUS)
    public void emailContactUS() {
        HideMenuDrawer();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.lbl_ContactUS))
                .setMessage("Do you want to contact us?")
                .setCancelable(false).
                setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendEmail(Constants.CONTACT_US_EMAIL,"");

                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        //  if (!alertDialog.isShowing()){
        // create alert dialog
        alertDialog = alertDialogBuilder.create();
        // show it

        alertDialog.show();
    }


    @OnClick(R.id.lnMore)
    public void openAppPlayStore(){
        showHideMenuDrawer();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    public void sendEmail(String mEmail, String mSubject){
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{mEmail});
        email.putExtra(Intent.EXTRA_SUBJECT, mSubject);
        email.setType("message/rfc822");
        try {
            startActivity(Intent.createChooser(email, "Choose an Email client :"));
        } catch (android.content.ActivityNotFoundException anfe) {
            Toast.makeText(getApplicationContext(), "Application not installed",Toast.LENGTH_LONG).show();
        }

    }
    public void openTopTrack(String title, int type, int cat_id, int SubCatID) {
        HideMenuDrawer();

        mFragment = new Fragment_TrackList();
        Bundle args = new Bundle();
        args.putString("Title", title);
        args.putInt("Type", type);
        args.putInt("cat_id",cat_id);
        args.putInt("SubCatID",SubCatID);
        mFragment.setArguments(args);
        showFragment(mFragment);
    }

    @OnClick(R.id.lntime)
    public void time() {
        if(mFragment instanceof  Fragment_TimeView) {
            return;
        }
        mFragment = new Fragment_TimeView();
        Bundle args = new Bundle();
        args.putString("Title", "TIME PAGE");
        mFragment.setArguments(args);
        showFragment(mFragment);
    }

    @OnClick(R.id.ivSetting)
    public void openSetting() {
        showHideMenuDrawer();
    }

    @OnClick(R.id.ivBack)
    public void back() {
        onBackPressed();
    }


    @OnClick(R.id.lnClose)
    public void closeSetting() {
        showHideMenuDrawer();
    }

    private void initDrawerLayout() {
        mDrawerLayout.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerOpened(View drawerView) {
                mDrawerLayout.requestFocus();
            }
        });

        final ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //ivMenu.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // ivMenu.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.coloryellow)));
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }


    public void showHideMenuDrawer() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    public void HideMenuDrawer() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }
    }

    private int mCount = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                L.d("Stack Count: " + fragmentManager.getBackStackEntryCount());
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    fragmentManager.popBackStack();
                    mCount = 0; // Reset mCount
                    return false;
                } else {
                    mCount++;
                    if (mCount == 1) {
                        return false;
                    }
                    if (mCount == 2) finish();
                }
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setupParent(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Utils.hideSoftKeyboard(MainActivity.this);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupParent(innerView);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        L.d("onResume");
    }

    public void showFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped) { //fragment not in back stack, create it.
            // set up transaction
            // fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.frame_container, fragment, backStateName).
                    addToBackStack(backStateName);
            fragmentTransaction.commit();
        }
    }

    public void setToolBarTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            txtSubTitle.setText(title);
            txtSubTitle.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.VISIBLE);
            ivSetting.setVisibility(View.GONE);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            txtSubTitle.setVisibility(View.GONE);
            ivBack.setVisibility(View.GONE);
            ivSetting.setVisibility(View.VISIBLE);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    private BillingProcessor bp;
    private void setUpInApp() {
        if (!BillingProcessor.isIabServiceAvailable(this)) {
            Utils.showDialog("In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16", this);
        }
        // bp = new BillingProcessor(this, "YOUR LICENSE KEY FROM GOOGLE PLAY CONSOLE HERE", this);
        bp = new BillingProcessor(this, LICENSE_KEY, new BillingProcessor.IBillingHandler() {
            @Override
            public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
                L.t(getApplicationContext(),productId + " Minute:" +purchedMinute);
                long miliseconds=purchedMinute*60000;
                timeCountInMilliSeconds=timeCountInMilliSeconds+miliseconds;
                SharedPrefsUtils.SavePrefSelected_SECONDS(getApplicationContext(), timeCountInMilliSeconds);
                time.setText(hmsTimeFormatter(timeCountInMilliSeconds));
            }

            @Override
            public void onBillingError(int errorCode, @Nullable Throwable error) {
                //   showToast("onBillingError: " + errorCode);
            }

            @Override
            public void onBillingInitialized() {
                L.t(getApplicationContext(),"onBillingInitialized");
                readyToPurchase = true;
                //bp.getPurchaseTransactionDetails("PRODUCT_ID");
               /* for(String sku : bp.listOwnedProducts())
                    L.d( "Owned Managed Product: " + );
                for(String sku : bp.listOwnedSubscriptions())
                    L.d("Owned Subscription: " + sku);*/
            }

            @Override
            public void onPurchaseHistoryRestored() {
                //showToast("onPurchaseHistoryRestored");
                for (String sku : bp.listOwnedProducts())
                    L.d("Owned Managed Product: " + sku);
                for (String sku : bp.listOwnedSubscriptions())
                    L.d("Owned Subscription: " + sku);
            }
        });
    }

    int purchedMinute=0;
    public void purchaseMinute(String product, int minute){
        if (!readyToPurchase) {
            Utils.showDialog("Billing not initialized.",this);
            return;
        }
        purchedMinute=minute;
        bp.purchase(this,product);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        mFragment.onActivityResult(requestCode,requestCode,data);
        if (!bp.handleActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data);
    }
    /**
     * method to convert millisecond to time format
     *
     * @param milliSeconds
     * @return HH:mm:ss time formatted string
     */
    public String hmsTimeFormatter(long milliSeconds) {

        String hms = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(milliSeconds),
                TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliSeconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliSeconds)));

        return hms;
    }

    public void syncToken(){

        if(TextUtils.isEmpty(NOTIFICATION_SERVICES_ID)) {
            NOTIFICATION_SERVICES_ID = FirebaseInstanceId.getInstance().getToken();
            L.e("ToKEN=> " + NOTIFICATION_SERVICES_ID);
        }
        HashMap<String, String> headers =new HashMap<>();
        headers.put("un", Constants.AUTH_EMAIL);
        headers.put("psw",Constants.AUTH_Password);
        ServicesClient.getInstance().makeRequest(headers);
        Call<Categories>  call=ServicesClient.getInstance().getServices().
                syncToken(BaseActivity.NOTIFICATION_SERVICES_ID, "android");

        call.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {

            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {

            }
        });
    }

}

