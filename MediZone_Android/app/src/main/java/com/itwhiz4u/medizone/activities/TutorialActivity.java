package com.itwhiz4u.medizone.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.adapter.CustomViewPagerAdapter;
import com.itwhiz4u.medizone.logging.L;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TutorialActivity extends BaseActivity {

    @BindView(R.id.pager) ViewPager mPager;
    private int mUIOptions, mCurrentItem, mTotalItems, mNextItem;
    @BindView(R.id.circlePageIndicator)
    CirclePageIndicator mIndicator;

    @BindView(R.id.ln1)
    FrameLayout ln1;
    @BindView(R.id.tvDone)
    TextView tvDone;
    @BindView(R.id.ivBack)
    ImageButton ivBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        initData();
        initListener();
        if(TextUtils.isEmpty(NOTIFICATION_SERVICES_ID)) {
            NOTIFICATION_SERVICES_ID = FirebaseInstanceId.getInstance().getToken();
            L.e("ToKEN=> " + NOTIFICATION_SERVICES_ID);
        }
    }

    private void initData() {
        //Bind the title mIndicator to the adapter
        mPager.setAdapter(new CustomViewPagerAdapter(getSupportFragmentManager()));
        mIndicator.setPageColor(ContextCompat.getColor(this, R.color.transparent));
        mIndicator.setViewPager(mPager);
        mIndicator.setFillColor(ContextCompat.getColor(this, R.color.white));
    }


    private void initListener() {
        mIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentItem = mPager.getCurrentItem();
                mTotalItems = mPager.getAdapter().getCount();
                mNextItem = (mCurrentItem + 1) % mTotalItems;
                mPager.setCurrentItem(mNextItem);
            }
        });


        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    //  ln1.setVisibility(View.VISIBLE);
                    tvDone.setVisibility(View.VISIBLE);
                    ivBack.setVisibility(View.VISIBLE);
                    //checkBox1.setVisibility(View.VISIBLE);

                }else {
                    //  ln1.setVisibility(View.INVISIBLE);
                  //  checkBox1.setVisibility(View.GONE);
                    tvDone.setVisibility(View.VISIBLE);
                    ivBack.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @OnClick({R.id.tvDone, R.id.ivBack, R.id.ivClose})
    public void Done(){
        ActivityCompat.finishAffinity(this);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        finish();
    }
}
