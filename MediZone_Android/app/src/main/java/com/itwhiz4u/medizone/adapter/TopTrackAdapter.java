package com.itwhiz4u.medizone.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.interfaces.IImpostoActionListner;
import com.itwhiz4u.medizone.model.songs.Datum;

import java.util.ArrayList;

/**
 * Created by 3rddigital-001 on 12/07/17.
 */

public class TopTrackAdapter extends RecyclerView.Adapter<TopTrackAdapter.ViewHolder> {
    private Context context;
    private IImpostoActionListner listener;
    ArrayList<Datum> mCategoryItem;
    private int flag=3;

    public TopTrackAdapter(Context context, ArrayList<Datum> CategoryItem, IImpostoActionListner listener, int flg) {
        this.mCategoryItem = CategoryItem;
        this.context = context;
        this.listener =listener;
        this.flag=flg;
    }

    @Override
    public TopTrackAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_track, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TopTrackAdapter.ViewHolder viewHolder, final int i) {

        final Datum songitem=mCategoryItem.get(i);

        viewHolder.tvIndex.setText(""+(i+1));
        viewHolder.tvIndex1.setText(""+(i+1));
        viewHolder.itemTitle.setText(songitem.getName());
        if(flag==0){
            if(songitem.getIsTop()==1){
                viewHolder.image.setVisibility(View.VISIBLE);
                if(songitem.getStatus().equals("rising")) {
                    Glide.with(context).load(R.drawable.ic_songs_rising).
                            into(viewHolder.image);
                } else if(songitem.getStatus().equals("falling")){
                    Glide.with(context).load(R.drawable.ic_falling).
                            into(viewHolder.image);
                }else if(songitem.getStatus().equals("stagnant")){
                    Glide.with(context).load(R.drawable.ic_minues).
                            into(viewHolder.image);
                }
            }else
                viewHolder.image.setVisibility(View.GONE);
        }else {
            viewHolder.image.setVisibility(View.GONE);
        }
       // Glide.with(context).load( clientItem.image).into(viewHolder.image);
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClickListner(i, songitem.getName(), songitem.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCategoryItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView itemTitle, tvIndex, tvIndex1;
        public ImageView image;
        public LinearLayout linearLayout;
        public ViewHolder(View view) {
            super(view);
            this.itemTitle = (TextView) view.findViewById(R.id.title);
            this.tvIndex = (TextView) view.findViewById(R.id.tvIndex);
            this.tvIndex1 = (TextView) view.findViewById(R.id.tvIndex1);
            this.image = (ImageView) view.findViewById(R.id.itemImage);
            this.image.setVisibility(View.GONE);
            this.linearLayout=(LinearLayout) view.findViewById(R.id.relativeLayout1);
        }
    }

}
