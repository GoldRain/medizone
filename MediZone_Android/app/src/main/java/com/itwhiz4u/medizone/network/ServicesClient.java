package com.itwhiz4u.medizone.network;

import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itwhiz4u.medizone.utils.Constants;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServicesClient {
    public static ServicesClient mInstance;

    public static ServicesClient getInstance() {
        if (mInstance == null) {
            mInstance = new ServicesClient();
        }
        return mInstance;
    }

    /**
     * WEB SERVICES CONFIG
     */

    // server address
    public static final String API_GetAllCategory= "GetAllCategory";
    public static final String API_GetAllSubCategory = "GetAllSubCategory";

    public static final String API_GetTopSongs = "GetTopSongs";
    public static final String API_GetNewSongs = "GetNewSongs";
    public static final String API_GetSongs = "GetSongs";
    /*VARIABLES*/
    public IWebServices services;
    private Gson gson;
    private Interceptor interceptor;
    private Retrofit.Builder builder;

    public ServicesClient() {
        /* Create data factory. */
        String pattern = "yyyy-MM-dd'T'HH:mm:ssZ";
        gson = new GsonBuilder().registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .registerTypeHierarchyAdapter(List.class, new ItemTypeAdapterFactory.CollectionAdapter())
                .serializeNulls()
                .setDateFormat(pattern).create();

        builder = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());
    }

    public void makeRequest(HashMap<String, String> headers) {
        Retrofit retrofit;
        if (headers != null) {
            OkHttpClient client = createInterceptor(headers);
        /* Create Retrofit.*/
            retrofit = builder.client(client).build();
        } else {
            retrofit = builder.build();
        }
        services = retrofit.create(IWebServices.class);
    }

    public OkHttpClient createInterceptor(final HashMap<String, String> headers) {

          /*Add the interceptor to OkHttpClient*/
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (headers != null && headers.size() > 0) {

           //Define the interceptor, add authentication headers.*//*
            interceptor = new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request.Builder builder = chain.request().newBuilder();
               /*     for (String key : headers.keySet()) {
                        builder.addHeader(key, headers.get(key));
                    }*/
                    String credentials = headers.get("un") + ":" + headers.get("psw");
                    // create Base64 encodet string
                    final String basic =
                            "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    builder.addHeader("Authorization", basic);
                    return chain.proceed(builder.build());
                }
            };
            // Install the all-trusting trust manager
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addInterceptor(interceptor)
                    .readTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(2, TimeUnit.MINUTES)

                    .addInterceptor(loggingInterceptor);
  /*          String credentials = headers.get("un") + ":" + headers.get("psw");
            // create Base64 encodet string
            final String basic =
                    "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("Authorization", basic);
                    request.addHeader("Accept", "application/json");
                }
            });
*/

            return builder.build();
        }
        return builder.build();
    }

    public IWebServices getServices() {
        return services;
    }

    public static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            }};

            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting trust manager
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts,
                    new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .readTimeout(2, TimeUnit.MINUTES)
                    .writeTimeout(2, TimeUnit.MINUTES)
                    .connectTimeout(2, TimeUnit.MINUTES)
                    .sslSocketFactory(sslSocketFactory)
                    .addInterceptor(loggingInterceptor)
                    .hostnameVerifier(hostnameVerifier).build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
