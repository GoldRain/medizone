package com.itwhiz4u.medizone.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.itwhiz4u.medizone.R;
import com.itwhiz4u.medizone.activities.MainActivity;
import com.itwhiz4u.medizone.activities.RewardedVideoAdActivity;
import com.itwhiz4u.medizone.utils.SharedPrefsUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by rddigital-006 on 7/12/17.
 */

public class Fragment_TimeView extends Fragment {
    MainActivity mActivity;
    @BindView(R.id.time)
    TextView time;
    AdView mAdView;
     private Unbinder unbinder;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        return;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time_view, container, false);
        mActivity.setupParent(view);
        unbinder = ButterKnife.bind(this, view);
        time.setText(mActivity.hmsTimeFormatter(SharedPrefsUtils.getPrefSelected_SECONDS(getActivity())));

        initAds(view);
        return view;
    }



    @OnClick(R.id.btnVideo)
    public void openRewordVideo(){
        startActivityForResult(new Intent(mActivity, RewardedVideoAdActivity.class),1001);
    }

    @OnClick(R.id.btnPay1)
    public void pay1(){
        mActivity.purchaseMinute("android.test.purchased", 90);
    }

    @OnClick(R.id.btnPay5)
    public void pay5(){
        mActivity.purchaseMinute("android.test.canceled", 15*60);
    }

    @OnClick(R.id.btnPay15)
    public void pay15(){
        mActivity.purchaseMinute("android.test.refunded", -1);
    }
    private void initAds(View view) {
        mAdView = (AdView) view.findViewById(R.id.adView);
      //  mAdView.setAdSize(AdSize.BANNER);
      //  mAdView.setAdUnitId(getString(R.string.banner_home_footer));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getActivity(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getActivity(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getActivity(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);
    }
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
        unbinder.unbind();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setToolBarTitle(getArguments().getString("Title"));
        mActivity.mFragment=this;
        if (mAdView != null) {
            mAdView.resume();
        }
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}
